#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include "Statistics.h"

unsigned int calculateFilterf2(unsigned int f,unsigned int d){
    f = (unsigned int) ((float) f/(float) d);
    if(f==0 && d!=0 ){
        f=1;
    }
    return f;
}

unsigned int calculateFilterColumnd2(unsigned int f, unsigned int prevf, unsigned int columnf,unsigned int columnd){
    unsigned int newd;
    float base = (float) f/(float) prevf;
    float exponent = (float) columnf/ (float) columnd;
    newd = (unsigned int) ( (float) columnd * (1-pow(1-base,exponent))); 
    if(newd==0){
        newd=1;
    }
    return newd;
}

unsigned int calculateFilterf0(uint64_t u, uint64_t num,uint64_t previ,unsigned int f){
    float division = (float) (u-num)/(float) (u-previ);
    f = (unsigned int) (division* (float) f);
    if(f==0){
        f=1;
    }
    return f;
}

unsigned int calculateFilterd0(uint64_t u, uint64_t num,uint64_t previ,unsigned int d){
    float division = (float) (u-num)/(float) (u-previ);
    d = (unsigned int) (division* (float) d);
    if(d==0){
        d=1;
    }
    return d;
}

unsigned int calculateFilterColumnd0(unsigned int f,unsigned int prevf, unsigned int columnf,unsigned int d){
    float base = (float) f/(float) prevf;
    float exponent = (float) columnf/(float) d;
    d = (unsigned int) ((float) d *(1-pow(1-base,exponent))); 
    if(d==0){
        d=1;
    }
    return d;
}

unsigned int calculateFilterf1(uint64_t num,uint64_t i,uint64_t prevu,unsigned int f){
    float division = (float) (num-i)/(float) (prevu-i);
    f = (unsigned int) (division* (float) f);
    if(f==0){
        f=1;
    }
    return f;
}

unsigned int calculateFilterd1(uint64_t num,uint64_t i,uint64_t prevu,unsigned int d){
    float division = (float) (num-i)/(float) (prevu-i);
    d = (unsigned int) (division* (float) d);
    if(d==0){
        d=1;
    }
    return d;
}

unsigned int calculateRadixf(uint64_t u,uint64_t i,unsigned int fR,unsigned int fS){
    float n = u - i +1;
    float numenator = fR *fS;
    fR =(unsigned int) (numenator/n) ;
    if(fR==0){
        fR=1;
    }
    return fR;
}

unsigned int calculateRadixd(uint64_t u,uint64_t i,unsigned int dR,unsigned int dS){
    float n = u - i + 1.0;
    float numenator = dR *dS;
    dR =(unsigned int) (numenator/n) ;
    if(dR==0){
        dR=1;
    }
    return dR;
}

unsigned int calculateColumnsRadixd(unsigned int d,unsigned int prevdR,unsigned int columnf,unsigned int columnd){
    float base = (float) d/ (float) prevdR ;
    float exponent = (float)columnf/(float)columnd;
    //fprintf(stderr,"base is %f exponent is %f\n",base,exponent);
    columnd = (unsigned int) ((float) columnd * (1.0-pow(1.0-base,exponent))); 
    if(columnd==0){
        columnd=1;
    }
    return columnd;
}
