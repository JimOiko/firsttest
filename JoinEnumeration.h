#ifndef _JOIN_ENUMERATION_H
#define _JOIN_ENUMERATION_H

#include <stdint.h>
#include "StoreRelation.h"
#include "Input.h"


typedef struct BestTree{
    unsigned int* cost;
    int** path; // path has the relation Id of the relation in Question
}bestTree;

typedef struct CurrTree{
    unsigned int cost;
    int* path;
}currTree;

void StatsOfFilters(uint64_t num,int filterType,ChangingStats* TableTofilter,int columnId,int);

void StatisticsInRadix(int, int,ChangingStats* TableforStatsR,ChangingStats* TableforStatsS,int columnIdR,int columnIdS);

void StatsforSameTableCase(StoreTable TableToJoin,ChangingStats* TableforStatsR,int columnIdR,int columnIdS);

bestTree createBestTree(int** relationIdArray,unsigned int numberOfRelations,unsigned int* cost);

unsigned int** GetAllSubsetOfSizeR(int* set,int r,int setSize,int*);

void findCombinations(int* set,int setSize,int r,int index,unsigned int ** subsetArray,int i,int* pos ,unsigned int*);

int checkIfIdisInSet(int idToCheck,unsigned int* set,int setSize);

int checkForCrossProducts(unsigned int* set,int setSize,singlePredicate* predicateArray,int predicateArraySize);

int checkIfConnected(int idToCheck,unsigned int* set,int setSize,singlePredicate* predicateArray,int predicateArraySize);

int factorialUtil(int number);

unsigned int hashFunctionForBestTree(unsigned int* ,int);

int* joinEnumerationAlgorithm(int* relationIdArray,int arraySize,singlePredicate* predicateArray,int ,ChangingStats** statsToChange,StoreTable*,int*);

currTree createJoinTree(unsigned int treeCost, int* CurrentPath,int pathSize, int newId,int numOfColumnsOfNew,ChangingStats* statsOfNew,int columnOfNew,int numOfColumnsOfOld,ChangingStats* statsOfOld,int columnOfOld);

void getNewPredicateArray(singlePredicate*newpredicateArray,int*,singlePredicate* predicateArray, int*rightOrderArray,int sizeofOrder, int numberOfPredicates);

int* arraycheckIfConnected(int idToCheck,unsigned int* set,int setSize,singlePredicate* predicateArray,int predicateArraySize,int*sizetoreturn);


#endif