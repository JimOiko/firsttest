#ifndef _INTERMEDIATE_H
#define _INTERMEDIATE_H

typedef struct Intermediate{
    struct HeadList* resultList;
    int numOfRelations;
    struct Intermediate* nextIntermediate;
    struct Intermediate* previousIntermediate;
}intermediate;

typedef struct IntermediateArray{
    struct Intermediate* Head;
    struct Intermediate* Tail;
    int numberOfIntermediates;
}intermediateArray;

void createIntermediate(struct Intermediate**);

int isIdInIntermediate(intermediateArray*,int,int*);

void createRelationFromIntermediate(relation*, uint64_t*column,struct List*);

void addToIntermediate(struct Intermediate*,list*);

list* createNewIntermediateListFromFilter(list*,list*);

void createIntermediateArray(intermediateArray**);

void pushIntermediate(intermediateArray* IntermediateArray,intermediate* Intermediate);

intermediate* getIntermediateAtPos(intermediateArray*,int);

void DeleteIntermediateArray(intermediateArray* IntermediateArray);

void createNewIntermediateFromJoin(intermediate*,list*);

void DeleteIntermediate(intermediate* Intermediate);

void DeleteHeadListWithoutDestroyingList(headList* myHeadList);

void RemoveIntermediate(intermediateArray* IntermediateArray,int pos);




#endif
