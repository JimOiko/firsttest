#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#define N 3

//final



/*
typedef struct Histogram{
    unsigned int hashValue;
    unsigned int numberOfAppearances;
}hist;
*/

/*typedef struct PrefixSum{
    unsigned int hashValue;
    unsigned int startingPoint;
}psum;*/



unsigned int hashFunction1(int value);


void createHist(struct Relation* R,int ,int,int*);

void combineHistograms(int*,int**,int,int);

int* createPsum(int* histogram);

void createReorderedRelation(int* prefixSum,struct Relation* R,int,int, struct Relation* Rtonos );

void combineReorderedRelations();


#endif