#ifndef _RADIXJOIN_H
#define _RADIXJOIN_H
#include "List.h"


//final

/** Type definition for a tuple */
typedef struct Tuple {
    u_int64_t key;
    u_int64_t payload;
}tuple;

/**
* Type definition for a relation.
* It consists of an array of tuples and a size of the relation.
*/
typedef struct Relation {
    struct Tuple *tuples;
    unsigned int num_tuples;
}relation;

/**
* Type definition for a relation.
* It consists of an array of tuples and a size of the relation.
*/

typedef struct Result {
    int placeholder;
}result;

/** Radix Hash Join**/

int createTuple(struct Tuple*,u_int64_t key, u_int64_t payload);

tuple* createRelation(relation* myColumn,u_int64_t* columnRequested,int numRows);

void findingAllTuples(list* head1,list* head2,int** bucket,int** myChain,relation* S,relation* Rtonos,int* p);

void RadixHashJoin(list* resultsR,list* resultsS,relation* R,relation* S);

void iterateJoin(list* ,relation R,relation S);


#endif
