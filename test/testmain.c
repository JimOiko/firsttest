#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StoreRelation.h"
#include "RadixJoin.h"
#include "List.h"
#include "Input.h"
#include "Filter.h"
#include "Intermediate.h"
#include "CUnit/Basic.h"




/* The suite initialization function.
 * Opens the temporary file used by the tests.
 * Returns zero on success, non-zero otherwise.
 */
int init_suite1(void)
{
   
   return 0;
   
}


int clean_suite1(void)
{
   
   return 0;
   
}


void TestStore(){
    StoreTable* storeTable = malloc(2 * sizeof(StoreTable));
    u_int64_t** R1 = malloc(5*sizeof(u_int64_t*));
    for(int i = 0; i < 5; i++){
        R1[i] = malloc(10 * sizeof(u_int64_t));
        for(int j = 0; j < 10; j++){
            R1[i][j] = j*2;
        }
    }
    
    int index = 0;
    u_int64_t** tempTable = store(storeTable, R1,index,5,10);
    for(int i = 0; i < 5; i++){
        
        for(int j = 0; j < 10; j++){
            CU_ASSERT (j*2==tempTable[i][j]);
        }
    }
    
    index++;

    u_int64_t** S1 = malloc(3*sizeof(u_int64_t*));
    for(int i = 0; i < 3; i++){
        S1[i] = malloc(6 * sizeof(u_int64_t));
        for(int j = 0; j < 6; j++){
            S1[i][j] = j;
        }
    }
   
    tempTable = store(storeTable, S1, index, 3,6);
    for(int i = 0; i < 3; i++){    
        for(int j = 0; j < 6; j++){
            CU_ASSERT (j==tempTable[i][j]);
        }
    }
	

    relation myColumn; 

    tuple* tempTuple;
    tempTuple = createRelation(&myColumn, storeTable[0].tableOfPointers[0], (storeTable[0].numOfRows));

    for(int i = 0; i < storeTable[0].numOfRows; i++){
        CU_ASSERT (i*2 == tempTuple[i].key);
    }	

    
    for(int i = 0; i < 4; i++){
        CU_ASSERT((i*2) == createTuple(&(myColumn.tuples[i]),i*2,i));
    }

}


void TestFilterAndListResults(){

	u_int64_t** R = malloc(5*sizeof(u_int64_t*));
	for(int i = 0; i < 5; i++){
		R[i] = malloc(10 * sizeof(u_int64_t));
		for(int j = 0; j < 10; j++){
 			R[i][j] = j*1000;
		}	
	}
	
	relation myColumn; 
	createRelation(&myColumn, R[1], 10);
	
 	list* resultList = applyFilter(myColumn, 3000, 0,0);
	
	simpleNode* iterateNode = resultList->Head;
	for(int i = 0; i < resultList->size; i++){
		
		
		CU_ASSERT(i+4 == iterateNode->result);
			
		iterateNode = iterateNode->next;
 		
	}
 		
	

	list* headOfList;
	CU_ASSERT(0==createList(&headOfList,0));

	simpleNode* node;
	CU_ASSERT(NULL==createSimpleNode(&node));
  
     
        CU_ASSERT(10==addToNode(node,10));

	CU_ASSERT(111==pushResult(headOfList,111));
	CU_ASSERT(300==pushResult(headOfList,300));
	CU_ASSERT(1==pushResult(headOfList,1));


	
}


void TestPredicates(){

	char* predicates = "0.1=0.2&1.4=5.6&1.2<3000";
	
	
	CU_ASSERT(3 == getNumberOfPredicates(predicates));
	singlePredicate* predicateArray = (singlePredicate*) malloc(3 * sizeof(singlePredicate));
        getPredicate(predicates,predicateArray);
	

	CU_ASSERT(0 ==strcmp("0.1=0.2",predicateArray[0].predicate));
	CU_ASSERT(0 ==strcmp("1.4=5.6",predicateArray[1].predicate));
	CU_ASSERT(0 ==strcmp("1.2<3000",predicateArray[2].predicate));
	
   
    	char* line = "4 1 6|1.2=2.1|1.0 2.0";
	int numOfRelations=0;
	int* tablesInQuestion;      
        tablesInQuestion = returnArrayOfRelationsInQuestion(line,&numOfRelations);
	CU_ASSERT(tablesInQuestion[0] == 4);
	CU_ASSERT(tablesInQuestion[1] == 1);
	CU_ASSERT(tablesInQuestion[2] == 6);



}


void TestFilterIntermediates(){

	intermediateArray* intermediateArray;
 	createIntermediateArray(&intermediateArray);
	int pos;
	int positionInArray;
       


	intermediate* intermediateResults;
	            
        relation R;
	StoreTable* storeTable = malloc(2 * sizeof(StoreTable));
	u_int64_t** R1 = malloc(5*sizeof(u_int64_t*));
	for(int i = 0; i < 5; i++){
		R1[i] = malloc(10 * sizeof(u_int64_t));
		for(int j = 0; j < 10; j++){
		    R1[i][j] = j*2;
		}	
    	}
	    
	int index = 0;
    	store(storeTable, R1,index,5,10);

	createRelation(&R,storeTable[0].tableOfPointers[0],storeTable[0].numOfRows);
	
        CU_ASSERT(0 == createIntermediate(&intermediateResults));

        list* List = pushList(intermediateResults->resultList,applyFilter(R,4,0,0)); //bazw filtro >4
	
	simpleNode* tempNode = List->Head;
	for(int i =0;i < List->size; i++){
		CU_ASSERT(i+3 == tempNode->result);
		tempNode = tempNode->next;
	}
                
        CU_ASSERT(1 == pushIntermediate(intermediateArray,intermediateResults));
	
	
	pos=isIdInIntermediate(intermediateArray,0,&positionInArray);
	CU_ASSERT(0 == positionInArray);
	CU_ASSERT(0 == pos);	

 	intermediateResults=getIntermediateAtPos(intermediateArray,positionInArray);
	
        relation S;
                
        list* listToGet=getListAtPos(intermediateResults->resultList,pos);
	
	tempNode = listToGet->Head;
	for(int i=0; i < 7; i++){
		CU_ASSERT(i+3 == tempNode->result);
		tempNode = tempNode->next;		
	} 	
		
		
        relation Temp = createRelationFromIntermediate(&S,storeTable[0].tableOfPointers[1],listToGet);
        for(int i=0; i < 7; i++){
		CU_ASSERT(6+(i*2) == Temp.tuples[i].key);
		CU_ASSERT(i == Temp.tuples[i].payload);
	}     
   
        list* filterResults=applyFilter(S,12,1,0); // filtro <12
	tempNode = filterResults->Head;
	for(int i=0; i < 3; i++){
		CU_ASSERT(i == tempNode->result);
		tempNode = tempNode->next;
	}   	
		
        list* newList = createNewIntermediateListFromFilter(listToGet,filterResults);	
	tempNode = newList->Head;
	for(int i=0; i < 3; i++){
		CU_ASSERT(i+3 == tempNode->result);
		tempNode = tempNode->next;
	} 

        CU_ASSERT(NULL == DeleteList(listToGet));
		
                
        changeListAtPos(intermediateResults->resultList,pos,newList);
}


void TestJoinIntermediates(){
	StoreTable* storeTable = malloc(2 * sizeof(StoreTable));
	u_int64_t** R1 = malloc(5*sizeof(u_int64_t*));
	for(int i = 0; i < 5; i++){
		R1[i] = malloc(10 * sizeof(u_int64_t));
		for(int j = 0; j < 10; j++){
		    R1[i][j] = j*2;
		}	
    	}
	u_int64_t** S1 = malloc(5*sizeof(u_int64_t*));
	for(int i = 0; i < 5; i++){
		S1[i] = malloc(10 * sizeof(u_int64_t));
		for(int j = 0; j < 10; j++){
		    S1[i][j] = j;
		}	
    	}
    
	
    	store(storeTable, R1, 0, 5, 10);	
	store(storeTable, S1, 1, 5, 10);	
	
	intermediateArray* intermediateArray;
	createIntermediateArray(&intermediateArray);
	
	int positionInArrayR,positionInArrayS;
	isIdInIntermediate(intermediateArray,0,&positionInArrayR);
        isIdInIntermediate(intermediateArray,1,&positionInArrayS);


	intermediate* intermediateResults;
        relation R;
        relation S;
        createRelation(&R,storeTable[0].tableOfPointers[0],storeTable[0].numOfRows);
        createRelation(&S,storeTable[1].tableOfPointers[0],storeTable[1].numOfRows);
        createIntermediate(&intermediateResults);
        list* resultsR,*resultsS;
        createList(&resultsR,0);
        
       
        createList(&resultsS,1);
	RadixHashJoin(resultsR,resultsS,&R,&S);
	
	simpleNode* tempNode = resultsR->Head;
	for(int i=0; i<5;i++){
		CU_ASSERT(i == tempNode->result);
		tempNode = tempNode->next;       	
	}
	tempNode = resultsS->Head;
	for(int i=0; i<5;i++){

		CU_ASSERT(i*2 == tempNode->result);  
		tempNode = tempNode->next;      	
	}

        pushList(intermediateResults->resultList,resultsR);	//testarismena hdh
        
        pushList(intermediateResults->resultList,resultsS);
        
        pushIntermediate(intermediateArray,intermediateResults);
}


void TestIteratejoinAndNullResults(){

	StoreTable* storeTable = malloc(2 * sizeof(StoreTable));
	u_int64_t** R1 = malloc(5*sizeof(u_int64_t*));
	for(int i = 0; i < 5; i++){
		R1[i] = malloc(10 * sizeof(u_int64_t));
		for(int j = 0; j < 10; j++){
			if(i==1){
				R1[i][j] = j+100;
			}
			else{
		    		R1[i][j] = j*2;
			}
		}	
    	}

    	store(storeTable, R1, 0, 5, 10);

	//pass R to intermediate 
	intermediateArray* intermediateArray;
	createIntermediateArray(&intermediateArray);
	
	intermediate* intermediateResults;
	            //printf("filterype %d\n",filterType);
        relation R;
        createRelation(&R,storeTable[0].tableOfPointers[0],storeTable[0].numOfRows);
		
		
        createIntermediate(&intermediateResults);
        list* filter;
        filter=applyFilter(R,0,0,0);	//>0
       
        pushList(intermediateResults->resultList,filter);
                
        pushIntermediate(intermediateArray,intermediateResults);
        free(R.tuples);
	
	int positionInArrayR,positionInArrayS;
    	int posR=isIdInIntermediate(intermediateArray,0,&positionInArrayR);
	int posS=isIdInIntermediate(intermediateArray,0,&positionInArrayS);

		
	
	relation S;
	intermediate* intermediateResults1=getIntermediateAtPos(intermediateArray,positionInArrayR);
	list* listOfR = getListAtPos(intermediateResults1->resultList,posR);
	list* listOfS = getListAtPos(intermediateResults1->resultList,posS);
	list* resultsofIntermediate;
	createList(&resultsofIntermediate,0);
	u_int64_t* columnOfR = storeTable[0].tableOfPointers[0];
	u_int64_t* columnOfS = storeTable[0].tableOfPointers[1];
	createRelationFromIntermediate(&R,columnOfR,listOfR);
	createRelationFromIntermediate(&S,columnOfS,listOfS);
	iterateJoin(resultsofIntermediate,R,S);	//zero results must be produced
	CU_ASSERT(0 == resultsofIntermediate->size);

	free(S.tuples);

	u_int64_t** S1 = malloc(5*sizeof(u_int64_t*));
	for(int i = 0; i < 5; i++){
		S1[i] = malloc(10 * sizeof(u_int64_t));
		for(int j = 0; j < 10; j++){
		    S1[i][j] = j+5000;
		}	
    	}	
	store(storeTable, S1, 1, 5, 10);


	
	//join whith empty intermediateList
	posR=isIdInIntermediate(intermediateArray,0,&positionInArrayR);
	posS=isIdInIntermediate(intermediateArray,1,&positionInArrayS);
	
	
        createRelation(&S,storeTable[1].tableOfPointers[0],storeTable[1].numOfRows);
        

        intermediate* intermediateResults2=getIntermediateAtPos(intermediateArray,positionInArrayR);
        
        list* listToGet=getListAtPos(intermediateResults2->resultList,posR);
		
        createRelationFromIntermediate(&R,storeTable[0].tableOfPointers[0],listToGet);               
        list* resultsR,*resultsS;
        createList(&resultsR,0);
        createList(&resultsS,1);
             
        RadixHashJoin(resultsR,resultsS,&R,&S);
        free(R.tuples);
                
        createNewIntermediateFromJoin(intermediateResults,resultsR);
        DeleteList(resultsR);
        pushList(intermediateResults->resultList,resultsS);
        CU_ASSERT(1 == intermediateArray->numberOfIntermediates);
	CU_ASSERT(0 == intermediateArray->Head->resultList->pointertoFirstList->List->size);
	CU_ASSERT(0 == intermediateArray->Head->resultList->pointertoLastList->List->size);
	
        free(S.tuples);
               

}

int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */

   if ((NULL == CU_add_test(pSuite, "test of store()", TestStore)) || (NULL == CU_add_test(pSuite, "test of filterResults()", TestFilterAndListResults)) || (NULL == CU_add_test(pSuite, "test of predicates()", TestPredicates))|| (NULL == CU_add_test(pSuite, "test of intermediatesFilter()", TestFilterIntermediates)) || (NULL == CU_add_test(pSuite, "test of intermediatesJoin()", TestJoinIntermediates)) || (NULL == CU_add_test(pSuite, "test of iterateJoinAndNullresults()", TestIteratejoinAndNullResults)) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
