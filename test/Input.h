#ifndef _INPUT_H
#define _INPUT_H

typedef struct SinglePredicate{
    char* predicate;
    int typeOfPredicate; //0 for join -- 1 for filter
}singlePredicate;

void getJoinInfo(char* predicate,int* relationIdR,int* relationIdS,int* columnIdR,int*columnIdS);
void getProjectionInfo(char* projection,int* relationId,int* columnId);


void parseTable(FILE* f,StoreTable*,int);
char* parseQuestion(FILE* f,int* nchar);
int* returnArrayOfRelationsInQuestion(char* line,int* numOfRelations);
char* returnPredicate(char* line);
char* returnCheckSums(char* line);

int getNumberOfFilters(char* predicate,int*);


int getNumberOfPredicates(char* predicate);
void getPredicate(char* predicate,singlePredicate*);
int getSums(char*,char***);


#endif
