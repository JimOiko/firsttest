#ifndef _INDEX_H
#define _INDEX_H

#include "RadixJoin.h"
#include "HashTable.h"

//final


/*typedef struct Chain{
    int position;
    int pointerToPosition;
}chain;*/

int hashFunction2(int key);

void createIndex(struct Relation* R,int** bucket,int**,int* PrefixSum,int bucketNum);

void printIndex(int num_tuples, int* bucket,int* myChain,int bucketNum,int* p);

#endif