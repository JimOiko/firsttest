#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "RadixJoin.h"
#include "List.h"
//#include <unistd.h>


//Synarthseis gia thn lista List

int createList(list** newList,int id){
    *newList= (list*) malloc(sizeof(list));
    (*newList)->size=0;
    (*newList)->relationId=id;
    (*newList)->Head=NULL;
    (*newList)->Tail=NULL;

    return (*newList)->size;
}

void* createSimpleNode(simpleNode** newNode){
    *newNode = (simpleNode*) malloc(sizeof(simpleNode));
    (*newNode)->next=NULL;

    return (*newNode)->next;
}

int addToNode(simpleNode* newNode,u_int64_t result){
    newNode->result=result;    

    return newNode->result;
}

int pushResult(list* myList,u_int64_t result){
    if(myList->Head==NULL){
	
        simpleNode* firstNode;
        createSimpleNode(&firstNode);
        addToNode(firstNode,result);
        myList->Tail=firstNode;
	    myList->Head = firstNode;
        myList->size++;
    }
    else{
        simpleNode* lastNode = myList->Tail;
    
        simpleNode* newNode;
        createSimpleNode(&newNode);
        addToNode(newNode,result);
        lastNode->next=newNode;
        myList->Tail=newNode;
        myList->size++;
      
    }
    return myList->Tail->result;
}

void createHeadList(headList** newHeadList){
    *newHeadList=(headList*) malloc(sizeof(headList));
    (*newHeadList)->numOfLists=0;
    (*newHeadList)->pointertoFirstList=NULL;
    (*newHeadList)->pointertoLastList=NULL;
}

void createNode(node** newNode){
    *newNode = (node*) malloc(sizeof(node));
    (*newNode)->nextList=NULL;
    (*newNode)->List=NULL;
}

void addToListNode(node* newNode,list* listToAdd){
    newNode->List=listToAdd;
}

list* pushList(headList* myHeadList,list* myList){

    if(myHeadList->pointertoFirstList==NULL){
        node* firstNode;
        createNode(&firstNode);
        addToListNode(firstNode,myList);
        myHeadList->pointertoFirstList=firstNode;
        myHeadList->pointertoLastList=firstNode;
        myHeadList->numOfLists++;
    }
    else{
        node* lastNode=myHeadList->pointertoLastList;
        node* newNode;
        createNode(&newNode);
        addToListNode(newNode,myList);
        lastNode->nextList=newNode;
        myHeadList->pointertoLastList=newNode;
        myHeadList->numOfLists++;
    }

    return myHeadList->pointertoLastList->List;
}

list* getListAtPos(headList* myHeadList,int pos){
    if(pos>=myHeadList->numOfLists){
        printf("pos > myHeadList->numOfLists\n");
        return NULL;
    }
    else if(pos<0){
        printf("pos < 0 \n");
        return NULL;
    }
    else{
        node* tempNode = myHeadList->pointertoFirstList;
        for(int i=0;i<pos;i++){
            tempNode =tempNode->nextList;
        }
        return  tempNode->List;
    }
}


int changeListAtPos(headList* myHeadList,int pos,list* myList){
    if(pos>=myHeadList->numOfLists){
        printf("pos > myHeadList->numOfLists\n");
        return -1;
    }
    else if(pos<0){
        printf("pos < 0 \n");
        return -1;
    }
    else{
        node* tempNode = myHeadList->pointertoFirstList;
        for(int i=0;i<pos;i++){
            tempNode =tempNode->nextList;
        }
        tempNode->List = myList;
	return 1;
    }
}





void* DeleteList(list* myList){
   
    while(myList->Head != NULL){
        simpleNode* tempNode = myList->Head->next;
        free(myList->Head);
        myList->Head = tempNode;
    }
    free(myList);
    myList = NULL;
    return myList;	
}


void DeleteHeadList(headList* myHeadList){
    while(myHeadList->pointertoFirstList !=NULL){
        node* tempNode = myHeadList->pointertoFirstList->nextList;
        DeleteList(myHeadList->pointertoFirstList->List);
        free(myHeadList->pointertoFirstList);
        myHeadList->pointertoFirstList = tempNode;
    }
    free(myHeadList);
}
