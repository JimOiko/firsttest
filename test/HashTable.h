#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#define N 13

//final



/*
typedef struct Histogram{
    unsigned int hashValue;
    unsigned int numberOfAppearances;
}hist;
*/

/*typedef struct PrefixSum{
    unsigned int hashValue;
    unsigned int startingPoint;
}psum;*/



unsigned int hashFunction1(int value);


int* createHist(struct Relation* R);

int* createPsum(int* histogram);

struct Relation* createReorderedRelation(int* prefixSum,struct Relation* R);


#endif