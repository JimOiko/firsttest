#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StoreRelation.h"
#include "Input.h"



void parseTable(FILE* f,StoreTable* storeTable,int index){

    u_int64_t numTuples;
    u_int64_t numColumns;
    

    fread(&numTuples,sizeof(u_int64_t), 1, f);
    //printf("bBUFFER %s\n",buffer);
    //numTuples =  atoll(buffer);
    //printf("index is %d\n",index);
    //printf("NUMROWS IS %ld\n",numTuples);
    fread(&numColumns,sizeof(u_int64_t), 1, f);
    //printf("NUMROWS IS %ld\n",numColumns);
    u_int64_t** Table = (u_int64_t**) malloc(numColumns*sizeof(u_int64_t*));
    for(int i=0;i < numColumns;i++){
        Table[i] = (u_int64_t*) malloc(numTuples*sizeof(u_int64_t));
        for(int j=0;j < numTuples;j++){
            fread(&Table[i][j],sizeof(u_int64_t), 1, f);
            //printf("Table IS %ld\n",Table[i][j]);
        }
    }

    store(storeTable, Table, index, numColumns, numTuples);

    free(Table);
}


char* parseQuestion(FILE* f,int* nchar){
    
   

    /*char* tline=NULL;
    size_t tlen =0;
    int countOfChars = 0;
    int currentRead;
    while ((currentRead = getline(&tline, &tlen, f))!=-1) {
        countOfChars += currentRead;
        printf("line is %s\n",tline);
        
        char *token = strtok(tline,";\t\n\r");
        if (strcmp(tline,"F")==0){
            printf("break\n");
            break;
        }
        while(token!=NULL){
            token = strtok(NULL,";\t\n\r");
        }
    }*/

    char* nline=NULL;
    size_t nlen =0;
    /*char* setOfQuestions = malloc(countOfChars* sizeof(char));
    int index=0;

    while (getline(&nline, &tlen, f)!=-1) {
        printf("line is %s\n",nline);
        
        char *token = strtok(nline," .;\t\n\r");
        if (strcmp(nline,"F")==0){
            printf("break\n");
            break;
        }
        while(token!=NULL){
            setOfQuestions[index]=
            token = strtok(NULL," .;\t\n\r");
        }
    }*/
    
    if((*nchar=getline(&nline, &nlen, f))!=-1){
        char *token = strtok(nline,";\t\n\r");
        //printf("line is %s\n",token);
        if (strcmp(nline,"F")==0){
            printf("F\n");
            return token;
        }
        *nchar -= 2;
        return token;
    }
    return NULL;
  
}



int* returnArrayOfRelationsInQuestion(char* line,int* numOfRelations){
    char* temp = malloc(strlen(line)+1);
    strcpy(temp,line);
   
    char *token = strtok(temp,"|");
    
    char* temp1 = malloc(strlen(token)+1);
    strcpy(temp1,token);
    
    char* number = strtok(temp1," ");
    
    while(number!=NULL){
        (*numOfRelations)++;
        number = strtok(NULL," ");
        
    }

    int* idOfRelations= malloc((*numOfRelations)*sizeof(int));
    number = strtok(token," ");
    int index=0;
    while(number!=NULL){
        idOfRelations[index] = atoi(number);
        index++;
        number = strtok(NULL," ");  
    }
    free(temp);
    free(temp1) ;
    return idOfRelations;
}


char* returnPredicate(char* line){
    char* temp = malloc((strlen(line)+1)*sizeof(char));
    strcpy(temp,line);
    char* token =strtok(temp,"|");
    token=strtok(NULL,"|");
    char* predicate = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy(predicate,token);
    free(temp);
    return predicate;
}

char* returnCheckSums(char* line){
    char* temp=malloc((strlen(line)+1)*sizeof(char));
    strcpy(temp,line);
    char* token =strtok(temp,"|");
    token=strtok(NULL,"|");
    token = strtok(NULL,"\0");
    char* checksums = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy(checksums,token);
    free(temp);
    return checksums;
}

int getSums(char* string,char*** SumArray){
    char* temp = (char*)malloc((strlen(string)+1)*sizeof(char));
    strcpy(temp,string);
    int counter=0;
    char* token=strtok(temp," ");
    while(token!=NULL){
        counter++;
        token=strtok(NULL," ");
    }
    strcpy(temp,string);
    (*SumArray) = (char**) malloc(counter*sizeof(char*));
    token = strtok(temp," ");
    (*SumArray)[0] = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy((*SumArray)[0],token);
    for(int i=1;i<counter;i++){
        token=strtok(NULL," ");
        (*SumArray)[i] = (char*) malloc((strlen(token)+1)*sizeof(char));
        strcpy((*SumArray)[i],token);	
    }
    free(temp);
    return counter;

}

void getProjectionInfo(char* projection,int* relationId,int* columnId){
    char* temp = (char*) malloc((strlen(projection)+1)*sizeof(char));
    strcpy(temp,projection);
    char* token =strtok(temp,".");
    *relationId = atoi(token);
    token=strtok(NULL,"\0");
    *columnId=atoi(token);
    free(temp);
    return;
}


int getNumberOfFilters(char* predicate,int *numberOfJoins){
    int numOfFilters=0;
    char* singlePredicate;
    printf("eftasasasasa %s\n",predicate);
    char* temp = malloc(strlen(predicate)+1);
    for(int i = 0; i < strlen(predicate)+1; i++){
        temp[i] = predicate[i];
    }
    singlePredicate = strtok(temp,"&");

     
    while(singlePredicate!=NULL){
        int i=0;
        int dots=0;
        
	
        while(singlePredicate[i] != '\0'){
	  
            if(singlePredicate[i]=='.'){
                dots++;
            }
            i++;
	
        }
        if(dots<2){
            numOfFilters++;
        }
        else{
            (*numberOfJoins)++;
        }
        singlePredicate = strtok(NULL,"&");
    }
    return numOfFilters;
}

int getNumberOfPredicates(char* predicate){
    int numOfPredicates=0;
    char* singlePredicate;
   
    char* temp = malloc(strlen(predicate)+1);
    strcpy(temp,predicate);

    
    singlePredicate = strtok(temp,"&");

     
    while(singlePredicate!=NULL){
        
        
        
	
        numOfPredicates++;

        singlePredicate = strtok(NULL,"&");
    }
    free(temp);
    return numOfPredicates;
}


void getPredicate(char* predicate,singlePredicate* predicateArray){
    char* temp = malloc(strlen(predicate)+1);
    strcpy(temp,predicate);
    int counter =0;
    char* token=strtok(temp,"&");
    while(token!=NULL){
        predicateArray[counter].predicate= (char*)malloc((strlen(token)+1)*sizeof(char));
        strcpy(predicateArray[counter].predicate,token);
        int i=0;
        int dots=0;
        while(predicateArray[counter].predicate[i] != '\0'){

            if(predicateArray[counter].predicate[i]=='.'){
                dots++;
            }
            i++;
        }
        if(dots<2){
            predicateArray[counter].typeOfPredicate=1;
        }
        else{
            predicateArray[counter].typeOfPredicate=0;
        }

        counter++;
        token = strtok(NULL,"&");
    }
    free(temp);
    return;
}

void getJoinInfo(char* predicate,int* relationIdR,int* relationIdS,int* columnIdR,int*columnIdS){
    char* temp = (char*) malloc((strlen(predicate)+1)*sizeof(char));
	strcpy(temp,predicate);
	
	char* token;
	token = strtok(temp,".");
	*relationIdR = atoi(token);
	token = strtok(NULL,"=");
	*columnIdR = atoi(token);
	*relationIdS = atoi(strtok(NULL,"."));
	*columnIdS = atoi(strtok(NULL,"\0"));
	free(temp);
	return;
}
