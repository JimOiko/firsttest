#ifndef _LIST_H
#define _LIST_H

//final


typedef struct ListTuple{
    int rowId; //in assignments after this maybe will need array of RowIds here instead of 2 Rowids
    

}listTuple;

typedef struct HeadList{
    struct Node* pointertoFirstList;
    int numOfLists;
    struct Node* pointertoLastList;
}headList;

typedef struct Node{
    struct Node* nextList;
    struct List* List;
}node;




//Lista gia ta apotelesmata twn filtrwn
typedef struct List{
    struct SimpleNode* Head;
    struct SimpleNode* Tail;
    int size;
    int relationId;
}list;

typedef struct SimpleNode{
    struct SimpleNode* next;
    u_int64_t result;
  
}simpleNode;



int createList(list** newList,int );

void* createSimpleNode(struct SimpleNode**);

int addToNode(struct SimpleNode*,u_int64_t result);

int pushResult(struct List*,u_int64_t result);

void createHeadList(headList** newHeadList);

void createNode(struct Node**);

void addToListNode(struct Node*,struct List*);

list* pushList(struct HeadList*,struct List*);

list* getListAtPos(struct HeadList*,int pos);

int changeListAtPos(headList* myHeadList,int pos,list* myList);

void* DeleteList(list* myList);

void DeleteHeadList(headList* myHeadList);

#endif
