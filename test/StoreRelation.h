#ifndef _STORERELATION_H
#define _STORERELATION_H


typedef struct Table{
    int numOfRows;
    int numOfColumns;
    u_int64_t **tableOfPointers;
}StoreTable;



void createTables(StoreTable* storeTable);
u_int64_t** store(StoreTable* storeTable,u_int64_t** table,int index,int columns,int rows);
void DeleteStoreTable(StoreTable* storeTable,int numTables);
/*
typedef struct Head{
    int numOfRelations;
    struct Table *pointerFirst;
    struct Table *pointerLast;

}head;



typedef struct Table{
    int numOfRows;
    int numOfColumns;
    int **tableOfPointers;
    struct Table* pointerNext;
}storeTable;


void push();
*/

#endif
