#include <stdlib.h>
#include <stdio.h>
#include "RadixJoin.h"
#include "List.h"
#include "Intermediate.h"


void createIntermediateArray(intermediateArray** IntermediateArray){
    *IntermediateArray = (intermediateArray*) malloc(sizeof(intermediateArray));
    (*IntermediateArray)->Head = NULL;
    (*IntermediateArray)->Tail = NULL;
    (*IntermediateArray)->numberOfIntermediates = 0;
}

    
int createIntermediate(intermediate** Intermediate){
    (*Intermediate) = (intermediate*) malloc(sizeof(intermediate));	
    (*Intermediate)->numOfRelations=0;
    createHeadList(&(*Intermediate)->resultList);
    (*Intermediate)->nextIntermediate=NULL;
    (*Intermediate)->previousIntermediate=NULL;

    return (*Intermediate)->numOfRelations;
}

int isIdInIntermediate(intermediateArray* IntermediateArray,int id,int* positionInArray){
    //printf("intermediate has %d arrays\n",IntermediateArray->numberOfIntermediates);
    //printf("intermediate has %d lists \n",IntermediateArray->Head->resultList->numOfLists);
    if(IntermediateArray->Head==NULL){
	
        return -1;
    }
    intermediate* tempIntermediate = IntermediateArray->Head;
    for(int j=0;j<IntermediateArray->numberOfIntermediates;j++){
        *positionInArray=j;
        node* tempList = tempIntermediate->resultList->pointertoFirstList;
        for(int i=0;i<tempIntermediate->resultList->numOfLists;i++){
	        //printf("relatioid %d id %d\n",tempList->List->relationId,id);
            if(tempList->List->relationId==id){
                return i;
            }
            tempList=tempList->nextList;
        }
        tempIntermediate=tempIntermediate->nextIntermediate;
    }
    return -1;
}


relation createRelationFromIntermediate(relation* R,u_int64_t* column,list* rows){
    int size = rows->size;
    
    R->tuples = (tuple*) malloc(size*sizeof(tuple));
    simpleNode* tempNode=rows->Head;
  	
    for(int i=0;i<size;i++){
        R->tuples[i].key=column[tempNode->result];
        R->tuples[i].payload=i;	
        tempNode=tempNode->next;
    }
    R->num_tuples=size;
    return *R;
}

list* createNewIntermediateListFromFilter(list* oldIntermediateList, list* Results){
    list* newListForIntermediate;
    createList(&newListForIntermediate,Results->relationId);
    simpleNode* resultsNode=Results->Head;
    simpleNode* intermediateNode = oldIntermediateList->Head;
    int counter=0;
    while(resultsNode!=NULL){
        int positionAtIntermediate = resultsNode->result;
        while(counter<positionAtIntermediate){
            intermediateNode=intermediateNode->next;
            counter++;
        }
        pushResult(newListForIntermediate,intermediateNode->result);
        resultsNode=resultsNode->next;
    }
    return newListForIntermediate;
}



int pushIntermediate(intermediateArray* IntermediateArray,intermediate* Intermediate){
    
    if(IntermediateArray->Head == NULL){
        IntermediateArray->Head = Intermediate;
        IntermediateArray->Tail = IntermediateArray->Head;
        IntermediateArray->numberOfIntermediates = 1;

    }
    else{
        IntermediateArray->Tail->nextIntermediate = Intermediate;
        Intermediate->previousIntermediate=IntermediateArray->Tail;
        IntermediateArray->Tail = IntermediateArray->Tail->nextIntermediate;
	    IntermediateArray->numberOfIntermediates++;
    }
    return IntermediateArray->numberOfIntermediates;
    
}

intermediate* getIntermediateAtPos(intermediateArray* intermediateArray,int pos){
    int size = intermediateArray->numberOfIntermediates;
    if(pos<0){
        printf("ERROR IN getIntermediateAtPos pos<0");
        return NULL;
    }
    if(pos>size){
        printf("ERROR IN getIntermediateAtPos pos>size");
        return NULL;
    }
    intermediate* tempIntermediate = intermediateArray->Head;
    for(int i=0;i<pos;i++){
        tempIntermediate=tempIntermediate->nextIntermediate;
    }
    return tempIntermediate;
}

void createNewIntermediateFromJoin(intermediate* oldIntermediate,list* resultsFromIntermediate){
    
    node* tempListNode = oldIntermediate->resultList->pointertoFirstList;
    while(tempListNode!=NULL){
        //printf("count me !!!!!\n");
        list* newlist;
        createList(&newlist,tempListNode->List->relationId);
        simpleNode* tempNode = resultsFromIntermediate->Head;
	
//        simpleNode* intermediateNode = tempListNode->List->Head;
	
        while(tempNode!=NULL){
            int counter =0;
            simpleNode* intermediateNode = tempListNode->List->Head;
            int positionAtIntermediate = tempNode->result;
            //printf("COUNTE ME!!!!\n");
            while(counter<positionAtIntermediate){
                intermediateNode=intermediateNode->next;
                counter++;
            }
            //printf("pushing\n");
            pushResult(newlist,intermediateNode->result);
            tempNode=tempNode->next;
        }
        //printf("Entering Delete\n");
        DeleteList(tempListNode->List);
        tempListNode->List=newlist;
        tempListNode=tempListNode->nextList;
       
    }
}



void DeleteIntermediateArray(intermediateArray* IntermediateArray){
    while(IntermediateArray->Head!=NULL){
        intermediate* temp = IntermediateArray->Head->nextIntermediate;
        DeleteHeadList(IntermediateArray->Head->resultList);
        free(IntermediateArray->Head);
        IntermediateArray->Head = temp;
    }
    free(IntermediateArray);
}

void DeleteIntermediate(intermediate* Intermediate){
    DeleteHeadListWithoutDestroyingList(Intermediate->resultList);
    Intermediate->nextIntermediate = NULL; 
   
    free(Intermediate);
    
}

void DeleteHeadListWithoutDestroyingList(headList* myHeadList){
    while(myHeadList->pointertoFirstList !=NULL){
        node* tempNode = myHeadList->pointertoFirstList->nextList;
        myHeadList->pointertoFirstList->List = NULL;
        free(myHeadList->pointertoFirstList);
        myHeadList->pointertoFirstList = tempNode;
    }
    free(myHeadList);
}

void RemoveIntermediate(intermediateArray* IntermediateArray,int pos){
    int size = IntermediateArray->numberOfIntermediates;
    if(pos<0){
        printf("ERROR IN getIntermediateAtPos pos<0");
        return;
    }
    if(pos>size){
        printf("ERROR IN getIntermediateAtPos pos>size");
        return;
    }
    intermediate* tempIntermediate = IntermediateArray->Head;
    int i;
    for(i=0;i<pos;i++){
        tempIntermediate=tempIntermediate->nextIntermediate;
    }
    if(i == 0){    //prwtos kombos einai aytos poy prepei na diagrafei
        IntermediateArray->Head = tempIntermediate->nextIntermediate;
        tempIntermediate->nextIntermediate->previousIntermediate = NULL;
        DeleteIntermediate(tempIntermediate);
    
    }
    else if (i == size){
        IntermediateArray->Tail = tempIntermediate->previousIntermediate;
        tempIntermediate->previousIntermediate->nextIntermediate = NULL;
        DeleteIntermediate(tempIntermediate);
    }
    else{
        tempIntermediate->previousIntermediate->nextIntermediate = tempIntermediate->nextIntermediate;
        tempIntermediate->nextIntermediate->previousIntermediate = tempIntermediate->previousIntermediate;
        DeleteIntermediate(tempIntermediate);
    }
    return;
}
