#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "List.h"
#include "RadixJoin.h"
#include "HashTable.h"
#include "Index.h"


//final

int createTuple(tuple* myTuple,u_int64_t key, u_int64_t payload){

    myTuple -> key = key;
    myTuple -> payload = payload;
	
    return myTuple->key;

}


tuple* createRelation(relation* myColumn,u_int64_t* columnRequested,int numRows){

    myColumn -> tuples = malloc(numRows * sizeof(tuple));
	
    for(int i = 0; i < numRows; i++){

        myColumn -> tuples[i].key = columnRequested[i];
        myColumn -> tuples[i].payload = i; 
    }
    myColumn -> num_tuples = numRows;
    return myColumn -> tuples;
}

/*
result* RadixHashJoin(struct Relation *relR, struct Relation *relS){
    
}
*/


void findingAllTuples(list* head1,list* head2,int** bucket,int** myChain,relation* S,relation* Rtonos,int* p){
    for(int i=0; i< S->num_tuples; i++){
        int hash1Value = hashFunction1(S->tuples[i].key);
        
        int hash2Value = hashFunction2(S->tuples[i].key);
        //printf("hashValue1 is %d  hashValue2 is %d of Value %d\n",hash1Value,hash2Value,S->tuples[i].key);
        int index = bucket[hash1Value][hash2Value]-1;          // position where bucket shows
        if(index<0) {
            continue;
        }
       // printf("index is %d\n",index);
        if(Rtonos->tuples[index].key == S->tuples[i].key){
            //printf("FOUND MATCH\n");
           // printf("%d and %d  == %d and %d\n",Rtonos->tuples[index].key,Rtonos->tuples[index].payload,S->tuples[i].key,S->tuples[i].payload);
            pushResult(head1,S->tuples[i].payload);
            pushResult(head2,Rtonos->tuples[index].payload);
        }
        
        while(myChain[hash1Value][index-p[hash1Value]] >0){
            index = myChain[hash1Value][index-p[hash1Value]]-1;
            //printf("index in while is %d and Rtonos->tuples[index].payload %d\n",index,Rtonos->tuples[index].payload);
            if(Rtonos->tuples[index].key == S->tuples[i].key){
                //printf("%d and %d  == %d and %d\n",Rtonos->tuples[index].key,Rtonos->tuples[index].payload,S->tuples[i].key,S->tuples[i].payload);
                //printf("FOUND MATCH\n");
                pushResult(head1,S->tuples[i].payload);
                pushResult(head2,Rtonos->tuples[index].payload);
            }
            
        }

    }
}

void RadixHashJoin(list* resultsR,list* resultsS,relation* R,relation* S){
    int total_buckets =(int) pow(2,N);
    int whoHasIndex=0;
    
    int* h= createHist(R);
    int* hs = createHist(S);
    int* p = createPsum(h);
    int* ps = createPsum(hs);

    relation* Rtonos = createReorderedRelation(p,R);
    relation* Stonos = createReorderedRelation(ps,S);

    int** bucket = (int**) malloc(total_buckets*sizeof(int*));
    int** myChain = (int**) malloc(total_buckets*sizeof(int*));

    if(R->num_tuples>=S->num_tuples){
        for(int bucketNum=0;bucketNum<total_buckets;bucketNum++){
            createIndex(Rtonos,&bucket[bucketNum],&myChain[bucketNum],p,bucketNum);
        }
    }
    else{
        for(int bucketNum=0;bucketNum<total_buckets;bucketNum++){
            createIndex(Stonos,&bucket[bucketNum],&myChain[bucketNum],ps,bucketNum);
        }
        whoHasIndex = 1;
    }
    if(whoHasIndex==0){
        findingAllTuples(resultsS,resultsR,bucket,myChain,Stonos,Rtonos,p);
    }
    else{
        findingAllTuples(resultsR,resultsS,bucket,myChain,Rtonos,Stonos,ps);
    }
    free(Rtonos->tuples);
    free(Stonos->tuples);
    free(Rtonos);
    free(Stonos);
    free(h);
    free(hs);
    free(p);
    free(ps);
    for(int i=0;i<total_buckets;i++){
        free(myChain[i]);
        free(bucket[i]);
    }
    free(bucket);
    free(myChain); 
    return;
}

void iterateJoin(list* resultsOfIntermediate,relation R,relation S){
    int size=R.num_tuples;
    for(int i=0;i<size;i++){
        if(R.tuples[i].key==S.tuples[i].key){
            pushResult(resultsOfIntermediate,R.tuples[i].payload);
        }
    }
    return;
}

