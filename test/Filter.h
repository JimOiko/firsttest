#ifndef _FILTER_H
#define _FILTER_H

struct List* applyFilter(relation,u_int64_t,int,int);

void getFilterInfo(char* predicate,int* relationId,int* columnId,u_int64_t* number,int* filterType);
#endif
