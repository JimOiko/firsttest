#include <stdio.h>
#include <stdlib.h>
#include "StoreRelation.h"

#define RROWS 10
#define RCOLUMNS 5
#define SROWS 8
#define SCOLUMNS 3
#define TROWS 6
#define TCOLUMNS 2
#define VROWS 7
#define VCOLUMNS 4



/*void createTables(StoreTable* storeTable){
    int index = 0;
    //Creation Table R
    int** R= (int**) malloc(RCOLUMNS * sizeof(int*));
    for( int j=0; j < RCOLUMNS; j++){
        R[j]= (int*) malloc(RROWS * sizeof(int));
        for(int i = 0; i < RROWS; i++){
            //int input = rand()%1000000;
            R[j][i] = i*10;    
        }
    }
    store(storeTable, R, index, RCOLUMNS, RROWS);
    index++;

    //Creation Relation S
    int** S= (int**) malloc(SCOLUMNS * sizeof(int*));
    for( int j=0; j < SCOLUMNS; j++){
        S[j]= (int*) malloc(SROWS * sizeof(int));
        for(int i = 0; i < SROWS; i++){
            //int input = rand()%1000000;
            S[j][i] = i*5;    
        }
    }
    store(storeTable, S, index, SCOLUMNS, SROWS);
    index++;

    //Creation Relation T
    int** T= (int**) malloc(TCOLUMNS * sizeof(int*));
    for( int j=0; j < TCOLUMNS; j++){
        T[j]= (int*) malloc(TROWS * sizeof(int));
        for(int i = 0; i < TROWS; i++){
            //int input = rand()%1000000;
            T[j][i] = i*4;    
        }
    }
    store(storeTable, T, index, TCOLUMNS, TROWS);
    index++;

    //Creation Relation V
    int** V= (int**) malloc(VCOLUMNS * sizeof(int*));
    for( int j=0; j < VCOLUMNS; j++){
        V[j]= (int*) malloc(VROWS * sizeof(int));
        for(int i = 0; i < VROWS; i++){
            //int input = rand()%1000000;
            V[j][i] = i*20;    
        }
    }
    store(storeTable, V, index, VCOLUMNS, VROWS);
    index++; 
}*/

u_int64_t** store(StoreTable* storeTable,u_int64_t** table,int index,int columns,int rows){
    storeTable[index].numOfColumns = columns;
    storeTable[index].numOfRows = rows;
    storeTable[index].tableOfPointers = (u_int64_t**) malloc(columns * sizeof(u_int64_t*));
    for(int i = 0; i < columns; i++){
        storeTable[index].tableOfPointers[i] = table[i];
    }

    //printf("index %d kai prwto stoixeio %d\n",index,storeTable[index].tableOfPointers[0][0]);
    return  storeTable[index].tableOfPointers;
}

void DeleteStoreTable(StoreTable* storeTable,int numTables){
    for(int i=0; i<numTables;i++) {
        for (int j=0;j<storeTable[i].numOfColumns;j++){
            free(storeTable[i].tableOfPointers[j]);
        }
        free(storeTable[i].tableOfPointers);
    }
    free(storeTable);
}
