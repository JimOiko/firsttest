#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "RadixJoin.h"
#include "HashTable.h"

//final

unsigned int hashFunction1(int value){
    int hashValue=0;
    int mask;
    mask=(int) pow(2,N);

    hashValue = value%mask;
    return hashValue;
}

int* createHist(relation* R){
    int totalBuckets;
    totalBuckets = (int) pow(2,N);
    int* histogram;
    
    histogram = (int*) malloc(totalBuckets*sizeof(int));
    for(int i=0;i<totalBuckets;i++){
        histogram[i]=0;
    }
    for(int i=0;i<R->num_tuples;i++){
        unsigned int hashValue = hashFunction1(R->tuples[i].key);
        histogram[hashValue]++;
    }

    return histogram;
}

int* createPsum(int* histogram){
    int totalBuckets;
    totalBuckets = (int) pow(2,N);
    int* prefixSum;
    prefixSum = (int*) malloc(totalBuckets*sizeof(int));
    /*for(int i=0;i<totalBuckets;i++){
        prefixSum[i].hashValue=i;
    }*/
    prefixSum[0]=0;
    for(int i=1;i<totalBuckets;i++){
        prefixSum[i]=prefixSum[i-1] + histogram[i-1];
    }
    return prefixSum;
}

relation* createReorderedRelation(int* prefixSum,relation *R){
    relation* reorderedR;
    reorderedR = malloc(sizeof(relation));
    reorderedR->tuples = malloc(R->num_tuples * sizeof(tuple));
    reorderedR->num_tuples = R->num_tuples;
    int totalBuckets = (int) pow(2,N);
    //printf("totaalbuckets %d\n",totalBuckets);
    int * counterArray = (int*) malloc(totalBuckets*sizeof(int));
    for(int i=0;i<totalBuckets;i++){
        counterArray[i]=prefixSum[i];
    }
    for(int i=0;i<R->num_tuples;i++){
        
        unsigned int hashValue = hashFunction1(R->tuples[i].key);
        //printf("hashvalue1 %d\n",hashValue);
        int sp= counterArray[hashValue];
        reorderedR->tuples[sp].key = R->tuples[i].key;
        reorderedR->tuples[sp].payload = R->tuples[i].payload;
        counterArray[hashValue]++;
    }
    free(counterArray);
    //printf("den eftase\n");
    return reorderedR;
}

