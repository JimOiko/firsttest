CC = gcc
OBJECTS = HashTable.o RadixJoin.o Input.o Index.o List.o Filter.o Intermediate.o main.o StoreRelation.o Scheduler.o JoinEnumeration.o Statistics.o
FLAGS = -Wall -c -g -lm -pthread
EXEC = projectPart3
TESTS = TestP1 TestP2 TestP3 TestP4
VALGRIND_FLAGS = --leak-check=yes --error-exitcode=1 --leak-check=full --show-leak-kinds=all --track-origins=yes --trace-children=yes -v

project: $(OBJECTS)
	$(CC) -o $(EXEC) -g -pthread  $(OBJECTS) -lm

HashTable.o: HashTable.c
	$(CC) $(FLAGS) HashTable.c

RadixJoin.o: RadixJoin.c
	$(CC) $(FLAGS) RadixJoin.c

Index.o: Index.c
	$(CC) $(FLAGS) Index.c

Input.o: Input.c
	$(CC) $(FLAGS) Input.c

List.o: List.c
	$(CC) $(FLAGS) List.c

Filter.o: Filter.c
	$(CC) $(FLAGS) Filter.c

Intermediate.o: Intermediate.c
	$(CC) $(FLAGS) Intermediate.c

StoreRelation.o: StoreRelation.c 
	$(CC) $(FLAGS) StoreRelation.c 

Scheduler.o: Scheduler.c 
	$(CC) $(FLAGS) Scheduler.c

Statistics.o: Statistics.c
	$(CC) $(FLAGS) Statistics.c

JoinEnumeration.o: JoinEnumeration.c 
	$(CC) $(FLAGS) JoinEnumeration.c

main.o: main.c
	$(CC) $(FLAGS) main.c

clean:
	rm -f $(OBJECTS) $(EXEC) $(TESTS)

cleanT:
	rm -f $(OBJECTS) $(TESTS)

run:
	./harness small.init small.work small.result ./projectPart3

valgrind: 
	valgrind $(VALGRIND_FLAGS) ./harness small.init small.work small.result ./projectPart3


