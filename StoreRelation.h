#ifndef _STORERELATION_H
#define _STORERELATION_H
#include <stdint.h>

typedef struct Table{
    int numOfRows;
    int numOfColumns;
    uint64_t **tableOfPointers;
    struct Statistics* stats;
}StoreTable;

typedef struct Statistics{
    uint64_t  i;
    uint64_t u;
    unsigned int f;
    unsigned int d;
    unsigned char* distinct;
    int sizedistinct;
}Statistics;

typedef struct ChangingStats{
    
    uint64_t  i;
    uint64_t u;
    unsigned int f;
    unsigned int d;
    unsigned char* distinct;
}ChangingStats;



void CopyStats(StoreTable* storeTable,ChangingStats*** statsToChange,int* tablesInQuestion,int numOfRelations);
void FreeChangingStats(StoreTable* storeTable,ChangingStats** statsToChange,int* tablesInQuestion,int numOfRelations);

void createTables(StoreTable* storeTable);

uint64_t store(StoreTable* storeTable,uint64_t** table,int index,int columns,int rows);

void DeleteStoreTable(StoreTable* storeTable,int numTables);

void CreateStats(StoreTable* storeTable,uint64_t** table,int index,int columns,int rows);


void printStats(StoreTable* storeTable,ChangingStats** statsToChange,int* tablesInQuestion,int numOfRelations);



#endif
