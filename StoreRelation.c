#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "StoreRelation.h"

#define RROWS 10
#define RCOLUMNS 5
#define SROWS 8
#define SCOLUMNS 3
#define TROWS 6
#define TCOLUMNS 2
#define VROWS 7
#define VCOLUMNS 4



int N = 40000000;

uint64_t store(StoreTable* storeTable,uint64_t** table,int index,int columns,int rows){
    storeTable[index].numOfColumns = columns;
    storeTable[index].numOfRows = rows;
    storeTable[index].tableOfPointers = (uint64_t**) malloc(columns * sizeof(uint64_t*));
    for(int i = 0; i < columns; i++){
        storeTable[index].tableOfPointers[i] = table[i];
    }
    storeTable[index].stats=(Statistics*) malloc(columns*sizeof(Statistics));

    CreateStats(storeTable,table,index,columns,rows);

    return  storeTable[index].tableOfPointers[columns-1][rows-1];
}

void DeleteStoreTable(StoreTable* storeTable,int numTables){
    for(int i=0; i<numTables;i++) {
        for (int j=0;j<storeTable[i].numOfColumns;j++){
            free(storeTable[i].stats[j].distinct);
            free(storeTable[i].tableOfPointers[j]);
        }
        free(storeTable[i].stats);
        free(storeTable[i].tableOfPointers);
    }
    
    free(storeTable);
}

void CreateStats(StoreTable* storeTable,uint64_t** table,int index,int columns,int rows){
    uint64_t min,max;
    for(int i = 0; i < columns; i++){
        
        min = table[i][0];
        max = table[i][0];

        storeTable[index].stats[i].f=1;
        for(int j=1;j<rows;j++){
            if(table[i][j]<min){
                min=table[i][j];
            }
            if(table[i][j]>max){
                max=table[i][j];
            }
            storeTable[index].stats[i].f++;
        }
        storeTable[index].stats[i].i = min;
        storeTable[index].stats[i].u = max;
        int exceedesN = 0;

        unsigned char* distinctVal;
        int sizeofcharArray;

        if(storeTable[index].stats[i].u-storeTable[index].stats[i].i+1 > N){
            if(N % 8 != 0){
                sizeofcharArray = N/8 +1;
            }
            else{
                sizeofcharArray = N/8;
            }
            
            exceedesN = 1;

        }
        else{
            if((storeTable[index].stats[i].u-storeTable[index].stats[i].i+1) % 8 != 0){
                sizeofcharArray = ((storeTable[index].stats[i].u-storeTable[index].stats[i].i+1) / 8) +1;
            }
            else{
                sizeofcharArray = (storeTable[index].stats[i].u-storeTable[index].stats[i].i+1) / 8;
            }
           
        }
        storeTable[index].stats[i].sizedistinct = sizeofcharArray;
        distinctVal=malloc(sizeofcharArray*sizeof(unsigned char));
        for(int init=0; init<sizeofcharArray; init++){
            distinctVal[init]=0;
        }
        
        storeTable[index].stats[i].d = 0;
        int counter=0;
        for(int x=0;x<rows;x++){
            if(exceedesN==1){      
               
                unsigned int posincharArray = ((table[i][x]-(storeTable[index].stats[i].i % N)) / 8);
                
                unsigned int offsetinPosition = ((table[i][x]-(storeTable[index].stats[i].i % N)) % 8);

                unsigned char mask = 0x80; //10000000
                mask = mask >> offsetinPosition;
              
                
                distinctVal[posincharArray] = mask | distinctVal[posincharArray];
                
            }
            else{
                
                unsigned int posincharArray = (int)((table[i][x]-storeTable[index].stats[i].i) / 8);
                
                unsigned int offsetinPosition = (int)((table[i][x]-(storeTable[index].stats[i].i)) % 8);
                //fprintf(stderr,"posinchaarr %d offset %d size %d\n",posincharArray,offsetinPosition,sizeofcharArray);
                
                unsigned char mask = 0x80; //10000000
                mask = mask >> offsetinPosition;
              
                
                distinctVal[posincharArray] = mask | distinctVal[posincharArray];
                
            }
           
            counter++;
        }

        unsigned char mask = 0x80;
        int zeros = 0;
        for(int indexinchararray = 0 ;indexinchararray < sizeofcharArray; indexinchararray++){
            
            for(int k = 0; k<8; k++){

                unsigned char temp = distinctVal[indexinchararray];
                int postoMove = k;
                temp = temp << postoMove;
                
                temp = temp & mask;
                if(temp == 0x80){
                   // fprintf(stderr,"mphka st temp me temp %d kai d %d \n",temp,storeTable[index].stats[i].d);
                    storeTable[index].stats[i].d++;
                }
                else{
                    zeros++;
                }
            }
        }

        storeTable[index].stats[i].distinct = distinctVal;
    
    }
}


void CopyStats(StoreTable* storeTable,ChangingStats*** statsToChange,int* tablesInQuestion,int numOfRelations){
    (*statsToChange) = (ChangingStats**) malloc(numOfRelations * sizeof(ChangingStats*));
    for(int i =0; i< numOfRelations;i++){
        (*statsToChange)[i] = malloc(storeTable[tablesInQuestion[i]].numOfColumns*sizeof(ChangingStats));
        for(int j =0 ; j < storeTable[tablesInQuestion[i]].numOfColumns; j++){
            (*statsToChange)[i][j].i = storeTable[tablesInQuestion[i]].stats[j].i;
            (*statsToChange)[i][j].u = storeTable[tablesInQuestion[i]].stats[j].u;
            (*statsToChange)[i][j].f = storeTable[tablesInQuestion[i]].stats[j].f;
            (*statsToChange)[i][j].d = storeTable[tablesInQuestion[i]].stats[j].d;
            (*statsToChange)[i][j].distinct = (unsigned char*) malloc(storeTable[tablesInQuestion[i]].stats[j].sizedistinct* sizeof(unsigned char));
            for(int k = 0; k < storeTable[tablesInQuestion[i]].stats[j].sizedistinct; k++){
                (*statsToChange)[i][j].distinct[k] = storeTable[tablesInQuestion[i]].stats[j].distinct[k];
            }
           // fprintf(stderr,"tablesInQuestion[i] is %d and column is %d i,u,d,f after %d %d %d %d\n",tablesInQuestion[i],j, (int)(*statsToChange)[i][j].i,(int)(*statsToChange)[i][j].u,(int)(*statsToChange)[i][j].d,(int)(*statsToChange)[i][j].f);

        }
    }

}

void printStats(StoreTable* storeTable,ChangingStats** statsToChange,int* tablesInQuestion,int numOfRelations){
 /*   for(int i =0; i< numOfRelations;i++){
        for(int j =0 ; j < storeTable[tablesInQuestion[i]].numOfColumns; j++){
      
            fprintf(stderr,"final stats for relation is %d and column is %d i,u,d,f after %d %d %d %d\n",tablesInQuestion[i],j, (int)(statsToChange[i][j].i),(int)(statsToChange[i][j].u),(int)(statsToChange[i][j].d),(int)(statsToChange[i][j].f));
        }
    }*/
}

void FreeChangingStats(StoreTable* storeTable,ChangingStats** statsToChange,int* tablesInQuestion,int numOfRelations){
    for(int i =0; i< numOfRelations;i++){
        for(int j =0 ; j < storeTable[tablesInQuestion[i]].numOfColumns; j++){
      
            free(statsToChange[i][j].distinct);
        }
        free(statsToChange[i]);
    }
    free(statsToChange);
}