#ifndef _FILTER_H
#define _FILTER_H

struct List* applyFilter(relation,uint64_t,int,int);

void getFilterInfo(char* predicate,int* relationId,int* columnId,uint64_t* number,int* filterType);


#endif
