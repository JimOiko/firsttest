#ifndef _RADIXJOIN_H
#define _RADIXJOIN_H
#include <stdint.h>

#include "List.h"


//final

/** Type definition for a tuple */
typedef struct Tuple {
    uint64_t key;
    uint64_t payload;
}tuple;

/**
* Type definition for a relation.
* It consists of an array of tuples and a size of the relation.
*/
typedef struct Relation {
    struct Tuple *tuples;
    unsigned int num_tuples;
}relation;

/**
* Type definition for a relation.
* It consists of an array of tuples and a size of the relation.
*/

typedef struct Result {
    int placeholder;
}result;

/** Radix Hash Join**/

void createTuple(struct Tuple*,uint64_t key, uint64_t payload);

void createRelation(relation* myColumn,uint64_t* columnRequested,int numRows);

void findingAllTuples(list* head1,list* head2,int** bucket,int** myChain,relation* S,relation* Rtonos,int* p,int startPoint,int endPoint);
 
void RadixHashJoin(list* resultsR,list* resultsS,relation* R,relation* S,StoreTable TableR,StoreTable TableS,int columnIdR,int columnIdS);

void iterateJoin(list* ,relation R,relation S);

void printRelation(relation*);

#endif
