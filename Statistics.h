#ifndef _STATISTICS_H
#define _STATISTICS_H

unsigned int calculateFilterf2(unsigned int f,unsigned int d);

unsigned int calculateFilterColumnd2(unsigned int f, unsigned int prevf, unsigned int columnf,unsigned int columnd);

unsigned int calculateFilterf0(uint64_t u, uint64_t num,uint64_t previ,unsigned int f);

unsigned int calculateFilterd0(uint64_t u, uint64_t num,uint64_t previ,unsigned int f);

unsigned int calculateFilterColumnd0(unsigned int f,unsigned int prevf, unsigned int columnf,unsigned int d);

unsigned int calculateFilterf1(uint64_t num,uint64_t i,uint64_t prevu,unsigned int f);

unsigned int calculateFilterd1(uint64_t num,uint64_t i,uint64_t prevu,unsigned int d);

unsigned int calculateRadixf(uint64_t u,uint64_t i,unsigned int fR,unsigned int fS);

unsigned int calculateRadixd(uint64_t u,uint64_t i,unsigned int dR,unsigned int dS);

unsigned int calculateColumnsRadixd(unsigned int d,unsigned int prevdR,unsigned int columnf,unsigned int columnd);

#endif