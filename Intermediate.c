#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "StoreRelation.h"
#include "RadixJoin.h"
#include "List.h"
#include "Intermediate.h"



void createIntermediateArray(intermediateArray** IntermediateArray){
    *IntermediateArray = (intermediateArray*) malloc(sizeof(intermediateArray));
    (*IntermediateArray)->Head = NULL;
    (*IntermediateArray)->Tail = NULL;
    (*IntermediateArray)->numberOfIntermediates = 0;
}

    
void createIntermediate(intermediate** Intermediate){
    (*Intermediate) = (intermediate*) malloc(sizeof(intermediate));	
    (*Intermediate)->numOfRelations=0;
    createHeadList(&(*Intermediate)->resultList);
    (*Intermediate)->nextIntermediate=NULL;
    (*Intermediate)->previousIntermediate=NULL;
}

int isIdInIntermediate(intermediateArray* IntermediateArray,int id,int* positionInArray){
    if(IntermediateArray->Head==NULL){
	
        return -1;
    }
    intermediate* tempIntermediate = IntermediateArray->Head;

    for(int j=0;j<IntermediateArray->numberOfIntermediates;j++){
        *positionInArray=j;
        node* tempList = tempIntermediate->resultList->pointertoFirstList;
        for(int i=0;i<tempIntermediate->resultList->numOfLists;i++){
	        //printf("relatioid %d id %d\n",tempList->List->relationId,id);
            if(tempList->List->relationId==id){
                return i;
            }
            tempList=tempList->nextList;
        }
        tempIntermediate=tempIntermediate->nextIntermediate;
    }
    return -1;
}


void createRelationFromIntermediate(relation* R,uint64_t* column,list* rows){
    int size = rows->size;
    //fprintf(stderr,"Size of %d is %d\n",rows->relationId,size);
    //printList(rows);
    R->tuples = (tuple*) malloc(rows->totalElements*sizeof(tuple));
    simpleNode* tempNode=rows->Head;
  	int pos=0;
    int counter=0;
    for(int i=0;i<size;i++){
        //while(tempNode!=NULL){
        while(pos!=tempNode->index){
                //tempNode=tempNode->next;
                //pos=0;
            if(tempNode==NULL){
                //fprintf(stderr,"kati phge straba\n");
                break;
            }
            //fprintf(debugFile,"pos is %d and value is %ld\n",pos,tempNode->result[pos]);
            R->tuples[counter].key=column[tempNode->result[pos]];
            R->tuples[counter].payload=counter;	
            pos++;
            counter++;
        }
        pos=0;
        tempNode=tempNode->next;
    }
    R->num_tuples=rows->totalElements;
    return;
}

list* createNewIntermediateListFromFilter(list* oldIntermediateList, list* Results){
    
    list* newListForIntermediate;
    createList(&newListForIntermediate,Results->relationId);
    simpleNode* resultsNode=Results->Head;
    simpleNode* intermediateNode = oldIntermediateList->Head;
    //int counter=0;
    int posOfResults=0;
    while(resultsNode!=NULL){
        if(posOfResults==resultsNode->index){
            resultsNode=resultsNode->next;
            posOfResults=0;
        }
        if(resultsNode==NULL){
            break;
        }
        int positionAtIntermediate = resultsNode->result[posOfResults];
        int listNodeAtIntermediate = positionAtIntermediate/((1024*128)/sizeof(uint64_t));
        int positionAtNode = positionAtIntermediate - listNodeAtIntermediate*((1024*128)/sizeof(uint64_t));
        //go to listNodeAtIntermediate
        for(int i=0;i<listNodeAtIntermediate;i++){
            intermediateNode=intermediateNode->next;
        }
        //take the element at positionAtNode
        pushResult(newListForIntermediate,intermediateNode->result[positionAtNode]);
        posOfResults++;
        intermediateNode=oldIntermediateList->Head;
    }
    return newListForIntermediate;
}



void pushIntermediate(intermediateArray* IntermediateArray,intermediate* Intermediate){
    
    if(IntermediateArray->Head == NULL){
        IntermediateArray->Head = Intermediate;
        IntermediateArray->Tail = IntermediateArray->Head;
        IntermediateArray->numberOfIntermediates = 1;

    }
    else{
        IntermediateArray->Tail->nextIntermediate = Intermediate;
        Intermediate->previousIntermediate=IntermediateArray->Tail;
        IntermediateArray->Tail = IntermediateArray->Tail->nextIntermediate;
	    IntermediateArray->numberOfIntermediates++;
    }
    
}

intermediate* getIntermediateAtPos(intermediateArray* intermediateArray,int pos){
    int size = intermediateArray->numberOfIntermediates;
    if(pos<0){
        printf("ERROR IN getIntermediateAtPos pos<0");
        return NULL;
    }
    if(pos>size){
        printf("ERROR IN getIntermediateAtPos pos>size");
        return NULL;
    }
    intermediate* tempIntermediate = intermediateArray->Head;
    for(int i=0;i<pos;i++){
        tempIntermediate=tempIntermediate->nextIntermediate;
    }
    return tempIntermediate;
}

void createNewIntermediateFromJoin(intermediate* oldIntermediate,list* resultsFromIntermediate){
    
    node* tempListNode = oldIntermediate->resultList->pointertoFirstList;
    while(tempListNode!=NULL){
        //printf("count me !!!!!\n");
        list* newlist;
        createList(&newlist,tempListNode->List->relationId);
        simpleNode* tempNode = resultsFromIntermediate->Head;
	
        //simpleNode* intermediateNode = tempListNode->List->Head;
       // for(int j=0;j<resultsFromIntermediate->size;j++){
            int posOfResults=0;
            //while(tempNode!=NULL){
            for(int i=0;i<resultsFromIntermediate->size;i++){
                //int counter =0;
                while(posOfResults!=tempNode->index){
                    if(tempNode==NULL){
                        break;
                    }
                    simpleNode* intermediateNode = tempListNode->List->Head;
                    int positionAtIntermediate = tempNode->result[posOfResults];
                    while(positionAtIntermediate>=0){
                        positionAtIntermediate=positionAtIntermediate-intermediateNode->index;
                        if(positionAtIntermediate>=0){
                            intermediateNode=intermediateNode->next;
                        }
                    }
                    
                    //int listNodeAtIntermediate = positionAtIntermediate/((1024*128)/sizeof(uint64_t));
                    int positionAtNode = intermediateNode->index+positionAtIntermediate; //positionAtIntermediate - listNodeAtIntermediate*((1024*128)/sizeof(uint64_t));
                    //printf("COUNTE ME!!!!\n");
                    /*for(int i=0;i<listNodeAtIntermediate;i++){
                        intermediateNode=intermediateNode->next;
                    }*/
                    //printf("pushing\n");
                    pushResult(newlist,intermediateNode->result[positionAtNode]);
                    posOfResults++;
                    //intermediateNode=tempListNode->List->Head;
                }
                tempNode = tempNode ->next;
                posOfResults=0;
            }
        //printf("Entering Delete\n");
        DeleteList(tempListNode->List);
        tempListNode->List=newlist;
        tempListNode=tempListNode->nextList;
       
    }
}



void DeleteIntermediateArray(intermediateArray* IntermediateArray){
    while(IntermediateArray->Head!=NULL){
        intermediate* temp = IntermediateArray->Head->nextIntermediate;
        DeleteHeadList(IntermediateArray->Head->resultList);
        free(IntermediateArray->Head);
        IntermediateArray->Head = temp;

    }

    free(IntermediateArray);

}


void DeleteIntermediate(intermediate* Intermediate){
    DeleteHeadListWithoutDestroyingList(Intermediate->resultList);
    Intermediate->nextIntermediate = NULL; 
   
    free(Intermediate);
    
}

void DeleteHeadListWithoutDestroyingList(headList* myHeadList){
    while(myHeadList->pointertoFirstList !=NULL){
        node* tempNode = myHeadList->pointertoFirstList->nextList;
        myHeadList->pointertoFirstList->List = NULL;
        free(myHeadList->pointertoFirstList);
        myHeadList->pointertoFirstList = tempNode;
    }
    free(myHeadList);
}

void RemoveIntermediate(intermediateArray* IntermediateArray,int pos){
    int size = IntermediateArray->numberOfIntermediates;
    //fprintf(stderr,"pos is %d size is %d\n",pos,size);
    if(pos<0){
        //fprintf(stderr,"ERROR IN getIntermediateAtPos pos<0\n");
        return;
    }
    if(pos>size){
        //fprintf(stderr,"ERROR IN getIntermediateAtPos pos>size\n");
        return;
    }
    intermediate* tempIntermediate = IntermediateArray->Head;
    int i;
    for(i=0;i<pos;i++){
        tempIntermediate=tempIntermediate->nextIntermediate;
    }
    if(i == 0){    //prwtos kombos einai aytos poy prepei na diagrafei
        IntermediateArray->Head = tempIntermediate->nextIntermediate;
        tempIntermediate->nextIntermediate->previousIntermediate = NULL;
        DeleteIntermediate(tempIntermediate);
    
    }
    else if (i == size-1){
        IntermediateArray->Tail = tempIntermediate->previousIntermediate;
        tempIntermediate->previousIntermediate->nextIntermediate = NULL;
        DeleteIntermediate(tempIntermediate);
    }
    else{
        tempIntermediate->previousIntermediate->nextIntermediate = tempIntermediate->nextIntermediate;
        tempIntermediate->nextIntermediate->previousIntermediate = tempIntermediate->previousIntermediate;
        DeleteIntermediate(tempIntermediate);
    }
    return;
}
