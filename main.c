#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include "StoreRelation.h"
#include "Input.h"
#include "List.h"
#include "RadixJoin.h"
#include "Intermediate.h"
#include <string.h>
#include "Filter.h"
#include <unistd.h>
#include "Scheduler.h"
#include "JoinEnumeration.h"
#include "Statistics.h"


int main(int argc,char* argv[]){
    char** filePaths;
    filePaths=(char**) malloc(14*sizeof(char*));
    char* path = (char*) malloc(6*sizeof(char));
    char* buffer = (char*) malloc(6*sizeof(char));
    int counter=0;
    memset(path,'\0',6*sizeof(char));
    memset(buffer,'\0',6*sizeof(char));
    while(1){
        read(STDIN_FILENO,buffer,3);
        int i=2;
        while(buffer[i]!='\n'){
            i++;
            buffer[i]='\0';
            strcat(path,buffer);
            read(STDIN_FILENO,buffer,1);
            i=0;
        }
        buffer[i+1]='\0';
        strcat(path,buffer);
        if(!strcmp(path,"Done\n")){
            break;
        }
        filePaths[counter] = (char*) malloc((strlen(path)+1)*sizeof(char));
        memset(filePaths[counter],'\0',(strlen(path)+1)*sizeof(char));
        //path[strlen(path)-1]='\0';
        strncpy(filePaths[counter],path,(strlen(path)-1));        
        //strcpy(filePaths[counter],path);
        counter++;
        memset(path,'\0',6*sizeof(char));
    }
    StoreTable* storeTable = (StoreTable*) malloc(14*sizeof(StoreTable));
    FILE *f;
    //time_t start = 0;
	//time_t end = 0;
	//time_t elapsed = 0;
	//start = time(NULL);
    for(int i=0;i<14;i++){
        f = fopen(filePaths[i] ,"rb");
        if(f==NULL){            
            perror("Error at fopen ");
            return 0;
        }
        parseTable(f,storeTable,i);
        fclose(f);
    }
    //end = time(NULL);
    //elapsed = end - start;
    //int hours = (int)(elapsed/3600);
    //int mins = (int)((elapsed - (hours*3600))/60);
    //float sec = ((elapsed - hours*3600 - mins*60));
    //fprintf(stderr,"Everything stored in %d:%d:%f \n",hours,mins,sec);


    free(buffer);
    free(path);
    for(int i=0;i<14;i++){
        free(filePaths[i]);
    }
    free(filePaths);
    initialize(&queue);	
    pthread_mutex_init (& mtx , 0);
    pthread_mutex_init (& mtx1 , 0);
    pthread_mutex_init (& mtx2 , 0);
    pthread_mutex_init (& anothermtx , 0);
    pthread_mutex_init (& barriermtx , 0);
	pthread_cond_init (& cond_nonempty , 0);
    pthread_cond_init (& cond_barrier , 0);
    pthread_cond_init (& cond_nonfull,0);
    pthread_cond_init (& cond_obtain,0);
    numOfJobsExecuted=0;
    pthread_t *thread_pool= (pthread_t*) malloc(NUM_OF_THREADS*sizeof(pthread_t));
    for(int i=0; i < NUM_OF_THREADS;i++){
        pthread_create(&thread_pool[i],NULL,thread_routine,NULL);
    }
    //fprintf(stderr,"threads created\n");


    buffer = (char*) malloc(4096*sizeof(char));
    memset(buffer,'\0',4096*sizeof(char));
    int offset =0;
    int query_no=0;
    
    int counterbatch =0;

    while(1){
        memset(buffer,'\0',4096*sizeof(char));
        offset=0;
        while(1){
            if(read(STDIN_FILENO,buffer+offset,1)==0){
                Endofrequests=1;
                free(buffer);
                pthread_mutex_unlock(&mtx);
                pthread_cond_broadcast(&cond_nonempty);
                for (int i = 0; i < NUM_OF_THREADS; ++i){
                    pthread_join(thread_pool[i], 0);
                }
                pthread_mutex_destroy(& mtx);
                pthread_mutex_destroy(& mtx1);
                pthread_mutex_destroy(& mtx2);
                pthread_mutex_destroy(& anothermtx );
                pthread_mutex_destroy(& barriermtx);
                pthread_cond_destroy (& cond_nonempty );
                pthread_cond_destroy (& cond_barrier );
                pthread_cond_destroy (& cond_nonfull);
                pthread_cond_destroy (& cond_obtain);
                DeleteStoreTable(storeTable,14);
                DestroyList(&queue);
                free(thread_pool);
                //fprintf(stderr,"BYE BYE\n");
                exit(EXIT_SUCCESS);
            }
            if(buffer[offset]=='F'){
                offset++;
                read(STDIN_FILENO,buffer+offset,1);
                break;
            }
            offset++;
        }
        int numberOfLines = getNumberOfLines(buffer)-1;    
        //fprintf(stderr,"batch is\n%s\n",buffer);
        char** arrayOfLines = (char**) malloc(numberOfLines*sizeof(char*));
        parseQuestion(buffer,arrayOfLines,numberOfLines);
        for(int i=0;i<numberOfLines;i++){

            

            char* line = (char*) malloc((strlen(arrayOfLines[i])+1)*sizeof(char));
            memset(line,'\0',(strlen(arrayOfLines[i])+1)*sizeof(char));
            strcpy(line,arrayOfLines[i]);
            int numOfRelations=0;
            int* tablesInQuestion;
            tablesInQuestion = returnArrayOfRelationsInQuestion(line,&numOfRelations);
            //  printf("numf %d\n",numOfRelations);

            ChangingStats** statsToChange;
            CopyStats(storeTable,&statsToChange,tablesInQuestion,numOfRelations);



            char* predicate = returnPredicate(line);
            //fprintf(stderr,"lines is %s\n",line);
            //printf("numofchars are %d\n",numOfChars);
            //fprintf(stderr,"Predicate reurned is %s\n",predicate);

            intermediateArray* intermediateArray;
            createIntermediateArray(&intermediateArray);
            int numberOfPredicates = getNumberOfPredicates(predicate);
            int* relationIdArray = (int*) malloc(numOfRelations*sizeof(int));
            for(int i=0;i<numOfRelations;i++){
                relationIdArray[i]=i;
            }
            singlePredicate* predicateArray = (singlePredicate*) malloc(numberOfPredicates * sizeof(singlePredicate));
            getPredicate(predicate,predicateArray);
            //fprintf(stderr,"Entering Filter section\n");
            //apply filters first
            for(int i=0;i< numberOfPredicates;i++){
                int relationId,columnId,filterType;
                uint64_t number;
                if(predicateArray[i].typeOfPredicate == 1){
                    getFilterInfo(predicateArray[i].predicate,&relationId,&columnId,&number,&filterType);
                    int pos;
                    int positionInArray;
                    pos=isIdInIntermediate(intermediateArray,relationId,&positionInArray);
                    //printf("pos is %d\n",pos);
                    if(pos != -1){
                        //to filtro yparxei se kapoion intermediate
                        intermediate* intermediateResults=getIntermediateAtPos(intermediateArray,positionInArray);
                        relation R;
                        
                        list* listToGet=getListAtPos(intermediateResults->resultList,pos);
                
                
                
                        createRelationFromIntermediate(&R,storeTable[tablesInQuestion[relationId]].tableOfPointers[columnId],listToGet);
                        //fprintf(stderr,"relationId sent is %d\n",relationId);
                        
                        list* filterResults=applyFilter(R,number,filterType,relationId);//i put statsToChange[relationId] and not statsToCHange[tablesInQuestion[relationId]]  
                                                                                        //cause tablesInQuestion[relationId] might be 14 and only have 3 tables to make join (ex. 14 0 1|0.1=0.2 ....)
                        StatsOfFilters(number,filterType,statsToChange[relationId],columnId,storeTable[tablesInQuestion[relationId]].numOfColumns);
                        list* newList = createNewIntermediateListFromFilter(listToGet,filterResults);
                
            
                        free(R.tuples);

                        DeleteList(listToGet);
                        
                        changeListAtPos(intermediateResults->resultList,pos,newList);
                
                
                    }
                    else{
                        //to filtro einai kainourgio
                        intermediate* intermediateResults;
                        //printf("filterype %d\n",filterType);
                        relation R;
                        createRelation(&R,storeTable[tablesInQuestion[relationId]].tableOfPointers[columnId],storeTable[tablesInQuestion[relationId]].numOfRows);
                
                
                        createIntermediate(&intermediateResults);
                        list* filter;
                       //fprintf(stderr,"relationIdsent is %d\n",tablesInQuestion[relationId]);
                        filter=applyFilter(R,number,filterType,relationId);
                       // fprintf(stderr,"AFTER FILTER THINGS ARE LIKE THIS in column %d:\ni,u,d,f after %d %d %d %d\n",columnId,(int)statsToChange[relationId][columnId].i,(int)statsToChange[relationId][columnId].u,(int)statsToChange[relationId][columnId].d,(int)statsToChange[relationId][columnId].f);

                        
                        //printf("filter size %d\n",filter->size);
                        pushList(intermediateResults->resultList,filter);
                        
                        pushIntermediate(intermediateArray,intermediateResults);
                        free(R.tuples);
                       // fprintf(stderr,"filter Done\n");
                
                    }
                
                }
            }
            
            //start applying iterate joins
            for(int i=0;i<numberOfPredicates;i++){
                int relationIdR,columnIdR,relationIdS,columnIdS;

                if(predicateArray[i].typeOfPredicate==2){
                    getJoinInfo(predicateArray[i].predicate,&relationIdR,&relationIdS,&columnIdR,&columnIdS);
                    int posR,posS;
                    int positionInArrayR,positionInArrayS;
                    //printf("Going to check ids %d and %d \n",tablesInQuestion[relationIdR],tablesInQuestion[relationIdS]);
                    posR=isIdInIntermediate(intermediateArray,relationIdR,&positionInArrayR);
                    posS=isIdInIntermediate(intermediateArray,relationIdS,&positionInArrayS);

                    //OYTE O R OYTE O S EINAI SE INTERMEDIATE
                    if(posR==-1 /*&& posS==-1*/){
                        intermediate* intermediateResults;
                        relation R;
                        relation S;
                        createRelation(&R,storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR],storeTable[tablesInQuestion[relationIdR]].numOfRows);
                        createRelation(&S,storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS],storeTable[tablesInQuestion[relationIdS]].numOfRows);
                        createIntermediate(&intermediateResults);
                        list* resultsR;
                        createList(&resultsR,relationIdR);
                        iterateJoin(resultsR,R,S);
                        pushList(intermediateResults->resultList,resultsR);
                        pushIntermediate(intermediateArray,intermediateResults);
                        free(R.tuples);
                        free(S.tuples);
                    }
                    //O R EINAI SE INTERMEDIATE 
                    else{
                        relation R;
                        relation S;
                        intermediate* intermediateResults=getIntermediateAtPos(intermediateArray,positionInArrayR);
                        list* listOfR = getListAtPos(intermediateResults->resultList,posR);
                        list* listOfS = getListAtPos(intermediateResults->resultList,posS);
                        
                        list* resultsofIntermediate;
                        createList(&resultsofIntermediate,-1);
                        uint64_t* columnOfR = storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR];
                        uint64_t* columnOfS = storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS];
                        createRelationFromIntermediate(&R,columnOfR,listOfR);
                        createRelationFromIntermediate(&S,columnOfS,listOfS);
                        if(posR!=posS){
                            iterateJoin(resultsofIntermediate,R,S);
                            createNewIntermediateFromJoin(intermediateResults,resultsofIntermediate);
                            DeleteList(resultsofIntermediate);

                        }
                    }
                }
            }
            int* queryOrder;
           //fprintf(stderr,"Entering joinEnumeration\n");
            queryOrder=joinEnumerationAlgorithm(relationIdArray,numOfRelations,predicateArray,numberOfPredicates,statsToChange,storeTable,tablesInQuestion);
            free(relationIdArray);
            singlePredicate* orderedPredicateArray = (singlePredicate*) malloc(numberOfPredicates * sizeof(singlePredicate));
            int orderedPredicateArraySize;
            getNewPredicateArray(orderedPredicateArray,&orderedPredicateArraySize,predicateArray, queryOrder, numOfRelations, numberOfPredicates);
            free(queryOrder);
            //for(int i=0;i<orderedPredicateArraySize;i++){
                //fprintf(stderr,"%s ",orderedPredicateArray[i].predicate);
            //}
            //fprintf(stderr,"\n");
            //fprintf(stderr,"Entering Join Section\n");
            //start apllying joins
            for(int i=0;i<orderedPredicateArraySize;i++){
                int relationIdR,columnIdR,relationIdS,columnIdS;
                if(orderedPredicateArray[i].typeOfPredicate==0){
                    //fprintf(stderr,"COUNTE ME!!!\n");
                    getJoinInfo(orderedPredicateArray[i].predicate,&relationIdR,&relationIdS,&columnIdR,&columnIdS);
                    //fprintf(stderr,"%d %d %d %d \n",relationIdR,columnIdR,relationIdS,columnIdS);
                    int posR,posS;
                    int positionInArrayR,positionInArrayS;
                    //printf("Going to check ids %d and %d \n",tablesInQuestion[relationIdR],tablesInQuestion[relationIdS]);
                    posR=isIdInIntermediate(intermediateArray,relationIdR,&positionInArrayR);
                    posS=isIdInIntermediate(intermediateArray,relationIdS,&positionInArrayS);
                    //printf("posR %d kai posS %d\n",posR,posS);   

                    //OUTE O R OYTE O S EINAI SE INTERMEDIATE 
                    if(posR==-1 && posS==-1){
                        intermediate* intermediateResults;
                        relation R;
                        relation S;
                        createRelation(&R,storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR],storeTable[tablesInQuestion[relationIdR]].numOfRows);
                        createRelation(&S,storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS],storeTable[tablesInQuestion[relationIdS]].numOfRows);
                        createIntermediate(&intermediateResults);
                        list* resultsR,*resultsS;
                        createList(&resultsR,relationIdR);
                        //int flag=0; //0 = RadixHashJoin
                        //if(tablesInQuestion[relationIdR]!=tablesInQuestion[relationIdS]){
                        createList(&resultsS,relationIdS);
                        RadixHashJoin(resultsR,resultsS,&R,&S,storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],columnIdR,columnIdS);
                        //StatisticsInRadix(storeTable[tablesInQuestion[relationIdR]].numOfColumns,storeTable[tablesInQuestion[relationIdS]].numOfColumns,statsToChange[relationIdR],statsToChange[relationIdS],columnIdR,columnIdS);

                        //}
                       //else{
                        //    iterateJoin(resultsR,R,S);  //here for join two col of same table
                       //     StatsforSameTableCase(storeTable[tablesInQuestion[relationIdR]],statsToChange[relationIdR],columnIdR,columnIdS);
                         //   flag=1;
                        //}
                        pushList(intermediateResults->resultList,resultsR);
                        //if(flag==0){
                        //    fprintf(stderr,"pushing the other one too\n");
                        pushList(intermediateResults->resultList,resultsS);
                        //}
                       // fprintf(stderr,"num of lists %d\n",intermediateResults->resultList->numOfLists);
                        pushIntermediate(intermediateArray,intermediateResults);
                        free(R.tuples);
                        free(S.tuples);
                        //fprintf(stderr,"join done \n");
                
                    }
                    // O R EINAI SE INTERMEDIATE KAI O S DEN EINAI 
                    else if(posR!=-1 && posS==-1){
                        relation R;
                        relation S;
                        createRelation(&S,storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS],storeTable[tablesInQuestion[relationIdS]].numOfRows);
                        /*for(int j=0;j<S.num_tuples;j++){
                            printf("(%ld,%ld)\n",S.tuples[j].key,S.tuples[j].payload);
                        }*/

                        intermediate* intermediateResults=getIntermediateAtPos(intermediateArray,positionInArrayR);
                        //printf("positioninArrayR %d\n",positionInArrayR);
                        list* listToGet=getListAtPos(intermediateResults->resultList,posR);
                
                        createRelationFromIntermediate(&R,storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR],listToGet);
                        /*printf("---------------------------------------------------------\n");
                        for(int j=0;j<R.num_tuples;j++){
                            printf("(%ld,%ld)\n",R.tuples[j].key,R.tuples[j].payload);
                        }*/
                
                        // printf("relation sizeR %d and relation sizeS %d\n",R.num_tuples,S.num_tuples);
                        list* resultsR,*resultsS;
                        createList(&resultsR,relationIdR);
                        createList(&resultsS,relationIdS);
                        // printf("idR %d and idS %d \n",tablesInQuestion[relationIdR],tablesInQuestion[relationIdS]);
                        RadixHashJoin(resultsR,resultsS,&R,&S,storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],columnIdR,columnIdS);
                        //fprintf(stderr,"AFTER FILTER THINGS ARE LIKE THIS in column %d:\ni,u,d,f after %d %d %d %d\n",2,(int)statsToChange[relationIdR][2].i,(int)statsToChange[relationIdR][2].u,(int)statsToChange[relationIdR][2].d,(int)statsToChange[relationIdR][2].f);
                        //StatisticsInRadix(storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],statsToChange[relationIdR],statsToChange[relationIdS],columnIdR,columnIdS);

                        free(R.tuples);
                        //fprintf(stderr,"resultR size %d resultS size %d \n",resultsR->size,resultsS->size);
                        //sleep(5);
                        createNewIntermediateFromJoin(intermediateResults,resultsR);
                        DeleteList(resultsR);
                        pushList(intermediateResults->resultList,resultsS);
                        //  printf("size = %d\n",intermediateResults->resultList->numOfLists);
                        free(S.tuples);
                        //fprintf(stderr,"join done \n");


                    }

                    //O S EINAI SE INTERMEDIATE KAI O R DEN EINAI 
                    else if(posR==-1 && posS!=-1){
                        relation R;
                        relation S;
                        createRelation(&R,storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR],storeTable[tablesInQuestion[relationIdR]].numOfRows);

                        intermediate* intermediateResults=getIntermediateAtPos(intermediateArray,positionInArrayS);

                        list* listToGet=getListAtPos(intermediateResults->resultList,posS);

                        createRelationFromIntermediate(&S,storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS],listToGet);

                        list* resultsR,*resultsS;
                        createList(&resultsR,relationIdR);
                        createList(&resultsS,relationIdS);
                        RadixHashJoin(resultsR,resultsS,&R,&S,storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],columnIdR,columnIdS);
                        //StatisticsInRadix(storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],statsToChange[relationIdR],statsToChange[relationIdS],columnIdR,columnIdS);

                        free(S.tuples);
                        createNewIntermediateFromJoin(intermediateResults,resultsS);
                        DeleteList(resultsS);
                        pushList(intermediateResults->resultList,resultsR);
                        free(R.tuples);
                        //fprintf(stderr,"join done \n");
                    }
                    else{
                        //KAI OI DYO EINAI STON IDIO INTERMEDIATE 
                        if(positionInArrayR==positionInArrayS){
                            relation R;
                            relation S;
                            intermediate* intermediateResults=getIntermediateAtPos(intermediateArray,positionInArrayR);
                            list* listOfR = getListAtPos(intermediateResults->resultList,posR);
                            list* listOfS = getListAtPos(intermediateResults->resultList,posS);
                            
                            list* resultsofIntermediate;
                            createList(&resultsofIntermediate,-1);
                            uint64_t* columnOfR = storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR];
                            uint64_t* columnOfS = storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS];
                            createRelationFromIntermediate(&R,columnOfR,listOfR);
                            createRelationFromIntermediate(&S,columnOfS,listOfS);
                            if(posR!=posS){
                                iterateJoin(resultsofIntermediate,R,S);
                                createNewIntermediateFromJoin(intermediateResults,resultsofIntermediate);
                                DeleteList(resultsofIntermediate);

                            }
                            else{
                                list* resultsOfS;
                                createList(&resultsOfS,relationIdS);
                                RadixHashJoin(resultsofIntermediate,resultsOfS,&R,&S,storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],columnIdR,columnIdS);
                                //StatisticsInRadix(storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],statsToChange[relationIdR],statsToChange[relationIdS],columnIdR,columnIdS);

                                createNewIntermediateFromJoin(intermediateResults,resultsofIntermediate);
                                pushList(intermediateResults->resultList,resultsOfS);
                            }
                            free(R.tuples);
                            free(S.tuples);
                        //fprintf(stderr,"join done \n");
                        }
                        //KAI OI DYO EINAI SE INTERMEDIATE ALLA SE DIAFORETIKOUS 
                        else{
                            relation R;
                            uint64_t* columnOfR = storeTable[tablesInQuestion[relationIdR]].tableOfPointers[columnIdR];
                            relation S;
                            uint64_t* columnOfS = storeTable[tablesInQuestion[relationIdS]].tableOfPointers[columnIdS];
                            intermediate* intermediateContainingR = getIntermediateAtPos(intermediateArray,positionInArrayR);
                            intermediate* intermediateContainingS = getIntermediateAtPos(intermediateArray,positionInArrayS);
                            list* listOfR = getListAtPos(intermediateContainingR->resultList,posR);
                            list* listOfS = getListAtPos(intermediateContainingS->resultList,posS);
                            createRelationFromIntermediate(&R,columnOfR,listOfR);
                            createRelationFromIntermediate(&S,columnOfS,listOfS);
                            list* resultsOfR,*resultsOfS;
                            createList(&resultsOfR,tablesInQuestion[relationIdR]);
                            createList(&resultsOfS,tablesInQuestion[relationIdS]);
                            RadixHashJoin(resultsOfR,resultsOfS,&R,&S,storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],columnIdR,columnIdS);
                            //StatisticsInRadix(storeTable[tablesInQuestion[relationIdR]],storeTable[tablesInQuestion[relationIdS]],statsToChange[relationIdR],statsToChange[relationIdS],columnIdR,columnIdS);

                            free(R.tuples);
                            free(S.tuples);
                            createNewIntermediateFromJoin(intermediateContainingR,resultsOfR);

                            createNewIntermediateFromJoin(intermediateContainingS,resultsOfS);
                            //fprintf(stderr,"Reform complete\n");
                            DeleteList(resultsOfS);
                            DeleteList(resultsOfR);
                            node* tempNode =intermediateContainingS->resultList->pointertoFirstList;
                            while(tempNode!=NULL){
                                pushList(intermediateContainingR->resultList,tempNode->List);
                                tempNode=tempNode->nextList;
                            }
                            //fprintf(stderr,"push complete\n");
                            //fprintf(stderr,"Entering delete Intermediate\n");
                            RemoveIntermediate(intermediateArray,positionInArrayS);
                           // fprintf(stderr,"delete complete\n");
                            intermediateArray->numberOfIntermediates--;
                           // fprintf(stderr,"join done \n");

                        }
                    }
                }
            }
            //fprintf(stderr,"Going to Sums\n");
            char* checksums=returnCheckSums(line);
            char** Sums;
            int numOfProjections=getSums(checksums,&Sums);
            char** stringSums = (char**) malloc(numOfProjections*sizeof(char*));
            int resultSize=0;
            for(int i=0;i<numOfProjections;i++){
            
                //fprintf(stderr,"COUNTE ME!\n");
                stringSums[i] =(char*) malloc(15*sizeof(char));
                memset(stringSums[i],'\0',15*sizeof(char));
                int relationId,columnid;
                getProjectionInfo(Sums[i],&relationId,&columnid);
                //fprintf(stderr,"Got Projections\n");
                
                int nothing=100;
                //printf("I am asking %d \n",tablesInQuestion[relationId]);
                
                int pos = isIdInIntermediate(intermediateArray,relationId,&nothing);
                //fprintf(stderr,"pos is %d",pos);
                intermediate * intermediateResults = intermediateArray->Head;//getIntermediateAtPos(intermediateArray,nothing);
                //printf("%d\n",pos);
                //printf("%d\n",intermediateResults->resultList->numOfLists);
                list* listOfR = getListAtPos(intermediateResults->resultList,pos);
                relation R;
                uint64_t* column = storeTable[tablesInQuestion[relationId]].tableOfPointers[columnid];
                createRelationFromIntermediate(&R,column,listOfR);
                //fprintf(stderr,"created relation\n");
                uint64_t S=0;
                for(int j=0;j<R.num_tuples;j++){
                    S=S+R.tuples[j].key;
                }
                if(i!=numOfProjections-1){
                    if(S!=0){
                        sprintf(stringSums[i],"%ld ",S);
                    }
                    else{
                        sprintf(stringSums[i],"NULL ");
                    }
                }
                else{
                    if(S!=0){
                        sprintf(stringSums[i],"%ld",S);
                    }
                    else{
                        sprintf(stringSums[i],"NULL");
                    }
                }
                resultSize=resultSize+strlen(stringSums[i]);
                free(R.tuples);
                //fprintf(stderr,"Sum Complete\n");
            }
            char* results = (char*) malloc((resultSize+2)*sizeof(char));
            memset(results,'\0',(resultSize+2)*sizeof(char));
            for(int i=0;i<numOfProjections;i++){
                strcat(results,stringSums[i]);
            }
            strcat(results,"\n");
            strcat(results,"\0");
            for(int i=0;i<numOfProjections;i++){
                free(stringSums[i]);
            }
            free(stringSums);
            //fprintf(stderr,"%s\n",results);
            //exit(EXIT_SUCCESS);
            write(STDOUT_FILENO,results,strlen(results));
            free(results);
            free(checksums);
            for(int i=0;i<numOfProjections;i++){
                free(Sums[i]);
            }

            free(Sums);
            free(predicate);
            for(int i=0;i<numberOfPredicates;i++){
                free(predicateArray[i].predicate);
            }
            for(int i=0;i<orderedPredicateArraySize;i++){
                free(orderedPredicateArray[i].predicate);
            }
            free(orderedPredicateArray);
            free(predicateArray);
            free(line);
            DeleteIntermediateArray(intermediateArray);
            query_no++;
            
            //printStats(storeTable,statsToChange,tablesInQuestion,numOfRelations);
            FreeChangingStats(storeTable,statsToChange,tablesInQuestion,numOfRelations);           //HERE FREEING STATS FOR LINE
            free(tablesInQuestion);

        }
        for(int i=0;i<numberOfLines;i++){
            free(arrayOfLines[i]);
        }
        free(arrayOfLines);

        counterbatch++;
        if(counterbatch ==3){
         //   exit(0);
        }
    }
    //exit(EXIT_SUCCESS);
}