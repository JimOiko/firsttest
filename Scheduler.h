#ifndef _SCHEDULER_H
#define _SCHEDULER_H
#include <stdio.h>
#include <string.h>
#include <pthread.h>


#define NUM_OF_THREADS 1

extern int Endofrequests;
extern int Runningthread;
extern int numOfJobsExecuted;
extern int totalJobs;

typedef struct JobNode jobNode;
typedef struct JobList jobList;
typedef struct SchedulerQueue schedulerQueue;
typedef struct Job job;

typedef struct ArgumentForHist{
	int id;
	relation* R;
	relation* S;
	int startPointR;
	int endPointR;
	int startPointS;
	int endPointS;
	int* HistR;
	int* HistS;
}argumentForHist;

typedef struct ArgumentForReordering{
	int id;
	int* psumR;
	int* psumS;
	relation* R;
	relation* S;
	int startPointR;
	int endPointR;
	int startPointS;
	int endPointS;
	relation* Rtonos;
	relation* Stonos;
}argumentForReordering;

typedef struct ArgumentForJoin{
	int id;
	list* resultsOfIndexed;
	list* resultsOfS;
	int** bucket;
	int** myChain;
	int startPointS;
	int endPointS;
	relation* Relation;
	relation* RelationWithIndex;
	int* psumWithIndex;
}argumentForJoin;

struct Job{
	void (*function)(void*); 
	void* argument;
};

struct JobNode
{
	job task;
	jobNode* nextJob;
};

struct JobList
{
	jobNode* start;
	jobNode* end;
};


struct SchedulerQueue
{
	jobList *data;	
	int count;
};

pthread_mutex_t mtx,mtx1,mtx2,anothermtx,barriermtx;
pthread_cond_t cond_nonempty, cond_barrier , cond_obtain;
pthread_cond_t cond_nonfull ;
schedulerQueue queue ;


int numOfJobsExecuted;

void initialize ( schedulerQueue * queue );
void place ( schedulerQueue * queue , job task);
job obtain ( schedulerQueue * queue );
void Barrier(int);
void DestroyList(schedulerQueue * queue);
void* thread_routine(void*);
void HistogramJob(void*);
void PartitionJob(void*);
void JoinJob(void*);
void printArgHist(void*);
void printArgReord(void*);

#endif