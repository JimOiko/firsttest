#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "StoreRelation.h"
#include "List.h"
#include "RadixJoin.h"
#include "Filter.h"


//0 = ">" 1 = "<" 2 = "="

list* applyFilter(relation R,uint64_t num,int filterType,int relationId){
    int relationSize = R.num_tuples;
    list* results;
    createList(&results,relationId);

    for(int i=0;i<relationSize;i++){
	
        switch(filterType){
            case 0:
		
                if(R.tuples[i].key > num){
                    //fprintf(debugFile,"FOUND ONE NOOB!\n");
                    pushResult(results,R.tuples[i].payload);
                }
                break;
            case 1:
                if(R.tuples[i].key < num){
                    pushResult(results,R.tuples[i].payload);
                }
                break;
            case 2:
                if(R.tuples[i].key == num){
                    pushResult(results,R.tuples[i].payload);
                }
                break;
            default:
                printf("BAD FILTERTYPE\n");
        }
        
    }

    //fprintf(stderr,"AFTER FILTER THINGS ARE LIKE THIS in column %d:\ni,u,d,f after %d %d %d %d\n",columnId,(int)TableTofilter[columnId].i,(int)TableTofilter[columnId].u,(int)TableTofilter[columnId].d,(int)TableTofilter[columnId].f);

    return results;

}

void getFilterInfo(char* predicate,int* relationId,int* columnId,uint64_t* number,int* filterType){

	char* temp = (char*) malloc((strlen(predicate)+1)*sizeof(char));
	strcpy(temp,predicate);
	//fprintf(stderr,"predicate is %s\n",temp);
	char* token;
	token = strtok(temp,".");
	*relationId = atoi(token);
	token = strtok(NULL,"><=");
	*columnId = atoi(token);
    token=strtok(NULL,"");
	*number = atoll(token);
	
	for(int i = 0;i < (strlen(predicate)+1);i++){
		if(predicate[i] == '>'){
			*filterType = 0;
		}
		else if (predicate[i] == '<'){
			*filterType = 1;
		}
		else if(predicate[i] == '='){
			*filterType = 2;
		}

	}
	free(temp);
	return;
	
}
