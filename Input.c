#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "StoreRelation.h"
#include "Input.h"



void parseTable(FILE* f,StoreTable* storeTable,int index){

    uint64_t numTuples;
    uint64_t numColumns;
    //char buffer[64];

    fread(&numTuples,sizeof(uint64_t), 1, f);
    //printf("bBUFFER %s\n",buffer);
    //numTuples =  atoll(buffer);
    //printf("index is %d\n",index);
    //printf("NUMROWS IS %ld\n",numTuples);
    fread(&numColumns,sizeof(uint64_t), 1, f);
    //printf("NUMROWS IS %ld\n",numColumns);
    uint64_t** Table = (uint64_t**) malloc(numColumns*sizeof(uint64_t*));
    for(int i=0;i < numColumns;i++){
        Table[i] = (uint64_t*) malloc(numTuples*sizeof(uint64_t));
        for(int j=0;j < numTuples;j++){
            fread(&Table[i][j],sizeof(uint64_t), 1, f);
            //printf("Table IS %ld\n",Table[i][j]);
        }
    }

    store(storeTable, Table, index, numColumns, numTuples);

    free(Table);
}

int getNumberOfLines(char* batch){
    int counter=0;
    for(int i=0;i<strlen(batch);i++){
        if(batch[i]=='\n'){
            counter++;
        }
    }
    return counter;
}

void parseQuestion(char* batch,char** arrayOfLines,int numberOfLines){
    char* temp = (char*) malloc((strlen(batch)+1)*sizeof(char));
    strcpy(temp,batch);
    char* token = strtok(temp,"\n");
    int i=0;
    while(token!=NULL && strcmp(token,"F")){
        arrayOfLines[i] = (char*) malloc((strlen(token)+1)*sizeof(char));
        memset(arrayOfLines[i],'\0',(strlen(token)+1)*sizeof(char));
        strcpy(arrayOfLines[i],token);
        i++;
        token=strtok(NULL,"\n");
    }
    free(temp);
    return;
}



int* returnArrayOfRelationsInQuestion(char* line,int* numOfRelations){
    char* temp = malloc(strlen(line)+1);
    strcpy(temp,line);
   
    char *token = strtok(temp,"|");
    
    char* temp1 = malloc(strlen(token)+1);
    strcpy(temp1,token);
    
    char* number = strtok(temp1," ");
    
    while(number!=NULL){
        (*numOfRelations)++;
        number = strtok(NULL," ");
        
    }

    int* idOfRelations= malloc((*numOfRelations)*sizeof(int));
    number = strtok(token," ");
    int index=0;
    while(number!=NULL){
        idOfRelations[index] = atoi(number);
        index++;
        number = strtok(NULL," ");  
    }
    free(temp);
    free(temp1) ;
    return idOfRelations;
}


char* returnPredicate(char* line){
    char* temp = malloc((strlen(line)+1)*sizeof(char));
    strcpy(temp,line);
    char* token =strtok(temp,"|");
    token=strtok(NULL,"|");
    char* predicate = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy(predicate,token);
    free(temp);
    return predicate;
}

char* returnCheckSums(char* line){
    char* temp=malloc((strlen(line)+1)*sizeof(char));
    strcpy(temp,line);
    char* token =strtok(temp,"|");
    token=strtok(NULL,"|");
    token = strtok(NULL,"\0");
    char* checksums = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy(checksums,token);
    free(temp);
    return checksums;
}

int getSums(char* string,char*** SumArray){
    char* temp = (char*)malloc((strlen(string)+1)*sizeof(char));
    strcpy(temp,string);
    int counter=0;
    char* token=strtok(temp," ");
    while(token!=NULL){
        counter++;
        token=strtok(NULL," ");
    }
    strcpy(temp,string);
    (*SumArray) = (char**) malloc(counter*sizeof(char*));
    token = strtok(temp," ");
    (*SumArray)[0] = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy((*SumArray)[0],token);
    for(int i=1;i<counter;i++){
        token=strtok(NULL," ");
        (*SumArray)[i] = (char*) malloc((strlen(token)+1)*sizeof(char));
        strcpy((*SumArray)[i],token);	
    }
    free(temp);
    return counter;

}

void getProjectionInfo(char* projection,int* relationId,int* columnId){
    char* temp = (char*) malloc((strlen(projection)+1)*sizeof(char));
    strcpy(temp,projection);
    char* token =strtok(temp,".");
    *relationId = atoi(token);
    token=strtok(NULL,"\0");
    *columnId=atoi(token);
    free(temp);
    return;
}


int getNumberOfFilters(char* predicate,int *numberOfJoins){
    int numOfFilters=0;
    char* singlePredicate;
    //printf("eftasasasasa %s\n",predicate);
    char* temp = malloc(strlen(predicate)+1);
    for(int i = 0; i < strlen(predicate)+1; i++){
        temp[i] = predicate[i];
    }
    singlePredicate = strtok(temp,"&");

     
    while(singlePredicate!=NULL){
        int i=0;
        int dots=0;
        //int index=0; 
	
        while(singlePredicate[i] != '\0'){
	  
            if(singlePredicate[i]=='.'){
                dots++;
            }
            i++;
	
        }
        if(dots<2){
            numOfFilters++;
        }
        else{
            (*numberOfJoins)++;
        }
        singlePredicate = strtok(NULL,"&");
    }
    return numOfFilters;
}

int getNumberOfPredicates(char* predicate){
    int numOfPredicates=0;
    char* singlePredicate;
   
    char* temp = malloc(strlen(predicate)+1);
    strcpy(temp,predicate);

    
    singlePredicate = strtok(temp,"&");

     
    while(singlePredicate!=NULL){
        //int i=0;
        //int dots=0;
        //int index=0; 
	
        numOfPredicates++;

        singlePredicate = strtok(NULL,"&");
    }
    free(temp);
    return numOfPredicates;
}


void getPredicate(char* predicate,singlePredicate* predicateArray){
    char* temp = malloc((strlen(predicate)+1)*sizeof(char));
    strcpy(temp,predicate);
    int counter = 0;
    char* token=strtok(temp,"&");
    while(token!=NULL){
        predicateArray[counter].predicate= (char*)malloc((strlen(token)+1)*sizeof(char));
        strcpy(predicateArray[counter].predicate,token);
        //fprintf(stderr,"predicateArray[%d] is %s\n",counter,predicateArray[counter].predicate);
        int i=0;
        int dots=0;
        while(predicateArray[counter].predicate[i] != '\0'){

            if(predicateArray[counter].predicate[i]=='.'){
                dots++;
            }
            i++;
        }
        if(dots<2){
            predicateArray[counter].typeOfPredicate=1;
            //token = strtok(NULL,"&");
        }
        else{
            char* relationIdR;
            char* relationIdS;
            char* temp2= (char*) malloc((strlen(token)+1)*sizeof(char));
            strcpy(temp2,token);
            getRelationIdOfPredicate(&relationIdR,&relationIdS,temp2);
            if(!strcmp(relationIdR,relationIdS)){
                predicateArray[counter].typeOfPredicate=2;
            }
            else{
                predicateArray[counter].typeOfPredicate=0;
            }
            free(relationIdR);
            free(relationIdS);
            free(temp2);
            strcpy(temp,predicate);
            token= strtok(temp,"&");
            for(int i=0;i<counter;i++){
                token =strtok(NULL,"&");
            }
        }

        counter++;
        
        token = strtok(NULL,"&");
    }
    free(temp);
    return;
}






void getRelationIdOfPredicate(char** relationRId,char** relationSId,char* predicate){
    char* token=strtok(predicate,".");
    *relationRId = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy(*relationRId,token);
    token=strtok(NULL,"=");
    token=strtok(NULL,".");
    *relationSId = (char*) malloc((strlen(token)+1)*sizeof(char));
    strcpy(*relationSId,token);
    return;
}

void getJoinInfo(char* predicate,int* relationIdR,int* relationIdS,int* columnIdR,int*columnIdS){
    char* temp = (char*) malloc((strlen(predicate)+1)*sizeof(char));
	strcpy(temp,predicate);
	
	char* token;
	token = strtok(temp,".");
	*relationIdR = atoi(token);
	token = strtok(NULL,"=");
	*columnIdR = atoi(token);
	*relationIdS = atoi(strtok(NULL,"."));
	*columnIdS = atoi(strtok(NULL,"\0"));
	free(temp);
	return;
}
