#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "StoreRelation.h"
#include "RadixJoin.h"
#include "HashTable.h"
#include "Scheduler.h"

//final

unsigned int hashFunction1(int value){
    int hashValue=0;
    int mask;
    mask=(int) pow(2,N);

    hashValue = value%mask;
    return hashValue;
}

void createHist(relation* R,int startPoint,int endPoint,int* histogram){  
    for(int i=startPoint;i<endPoint;i++){
        unsigned int hashValue = hashFunction1(R->tuples[i].key);
        histogram[hashValue]++;
    }
}

void combineHistograms(int* histogram, int** histogramArray,int sizeOfArray,int total_buckets){
    for(int i=0;i<sizeOfArray;i++){
        for(int j=0;j<total_buckets;j++){
            histogram[j] = histogram[j] + histogramArray[i][j];
        }
    }
}

int* createPsum(int* histogram){
    int totalBuckets;
    totalBuckets = (int) pow(2,N);
    int* prefixSum;
    prefixSum = (int*) malloc(totalBuckets*sizeof(int));
    /*for(int i=0;i<totalBuckets;i++){
        prefixSum[i].hashValue=i;
    }*/
    prefixSum[0]=0;
    for(int i=1;i<totalBuckets;i++){
        prefixSum[i]=prefixSum[i-1] + histogram[i-1];
    }
    return prefixSum;
}

void createReorderedRelation(int* prefixSum,relation *R,int startPoint,int endPoint,relation* reorderedR){
    for(int i=startPoint;i < endPoint;i++){   
        unsigned int hashValue = hashFunction1(R->tuples[i].key);
        //printf("hashvalue1 %d\n",hashValue);
        pthread_mutex_lock(&mtx2);
        int sp = prefixSum[hashValue];
        reorderedR->tuples[sp].key = R->tuples[i].key;
        reorderedR->tuples[sp].payload = R->tuples[i].payload;
        prefixSum[hashValue]++;
        pthread_mutex_unlock(&mtx2);
    }
    //printf("den eftase\n");
}