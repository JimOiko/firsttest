#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "StoreRelation.h"
#include "RadixJoin.h"
#include "List.h"
//#include <unistd.h>


//Synarthseis gia thn lista List

void createList(list** newList,int id){
    *newList= (list*) malloc(sizeof(list));
    (*newList)->size=0;
    (*newList)->totalElements=0;
    (*newList)->relationId=id;
    (*newList)->Head=NULL;
    (*newList)->Tail=NULL;
}

void createSimpleNode(simpleNode** newNode){
    *newNode = (simpleNode*) malloc(sizeof(simpleNode));
    (*newNode)->result = (uint64_t*) malloc(1024*128);
    (*newNode)->next=NULL;
    (*newNode)->index=0;
}

void addToNode(simpleNode* newNode,uint64_t result){
    int position = newNode->index;
    newNode->result[position]=result; 
    newNode->index++;   
}

void pushResult(list* myList,uint64_t result){
    if(myList->Head==NULL){
	//lista einai adeia

        simpleNode* firstNode;
        createSimpleNode(&firstNode);
        addToNode(firstNode,result);
        myList->Tail=firstNode;
	    myList->Head = firstNode;
        myList->totalElements++;
        myList->size++;
    }
    else{
        simpleNode* lastNode = myList->Tail;
        int maxSize = (1024*128)/sizeof(uint64_t);
        if(lastNode->index==maxSize){
            //neos kombos
            simpleNode* newNode;
            createSimpleNode(&newNode);
            addToNode(newNode,result);
            lastNode->next=newNode;
            myList->Tail=newNode;
            myList->size++;
            myList->totalElements++;
        }
        else{
            addToNode(lastNode,result);
            myList->totalElements++;
        }
    }
}

void createHeadList(headList** newHeadList){
    *newHeadList=(headList*) malloc(sizeof(headList));
    (*newHeadList)->numOfLists=0;
    (*newHeadList)->pointertoFirstList=NULL;
    (*newHeadList)->pointertoLastList=NULL;
}

void createNode(node** newNode){
    *newNode = (node*) malloc(sizeof(node));
    (*newNode)->nextList=NULL;
    (*newNode)->List=NULL;
}

void addToListNode(node* newNode,list* listToAdd){
    newNode->List=listToAdd;
}

void pushList(headList* myHeadList,list* myList){

    if(myHeadList->pointertoFirstList==NULL){
        node* firstNode;
        createNode(&firstNode);
        addToListNode(firstNode,myList);
        myHeadList->pointertoFirstList=firstNode;
        myHeadList->pointertoLastList=firstNode;
        myHeadList->numOfLists++;
    }
    else{
        node* lastNode=myHeadList->pointertoLastList;
        node* newNode;
        createNode(&newNode);
        addToListNode(newNode,myList);
        lastNode->nextList=newNode;
        myHeadList->pointertoLastList=newNode;
        myHeadList->numOfLists++;
    }
}

list* getListAtPos(headList* myHeadList,int pos){
    if(pos>=myHeadList->numOfLists){
        printf("pos > myHeadList->numOfLists\n");
        return NULL;
    }
    else if(pos<0){
        printf("pos < 0 \n");
        return NULL;
    }
    else{
        node* tempNode = myHeadList->pointertoFirstList;
        for(int i=0;i<pos;i++){
            tempNode =tempNode->nextList;
        }
        return  tempNode->List;
    }
}


int changeListAtPos(headList* myHeadList,int pos,list* myList){
    if(pos>=myHeadList->numOfLists){
        printf("pos > myHeadList->numOfLists\n");
        return -1;
    }
    else if(pos<0){
        printf("pos < 0 \n");
        return -1;
    }
    else{
        node* tempNode = myHeadList->pointertoFirstList;
        for(int i=0;i<pos;i++){
            tempNode =tempNode->nextList;
        }
        tempNode->List = myList;
	return 1;
    }
}

void combineResults(list** resultsR,list** arrayOfSmallerResults, int sizeOfArray){
    int leftOver=0;
    //fprintf(stderr,"SizeOfArray is = %d \n",sizeOfArray);
    for(int i=0;i<sizeOfArray;i++){
        //fprintf(debugFile,"Printing Smaller Result at pos %d \n\n\n",i);
        //printList(arrayOfSmallerResults[i]);
        if(arrayOfSmallerResults[i]->totalElements!=0){
            (*resultsR)->Head = arrayOfSmallerResults[i]->Head;
            (*resultsR)->Tail = arrayOfSmallerResults[i]->Tail;
            (*resultsR)->size = arrayOfSmallerResults[i]->size;
            (*resultsR)->totalElements = arrayOfSmallerResults[i]->totalElements;
            leftOver=i+1;
            //fprintf(stderr,"Found first non empty result from job #%d continuing from %d\n",i,leftOver);
            break;
        }
        free(arrayOfSmallerResults[i]);
    }
    if(leftOver!=0){
        free(arrayOfSmallerResults[leftOver-1]);
    }
    else{
        (*resultsR)->size = 0;
        (*resultsR)->totalElements = 0;
        //fprintf(stderr,"My results is NULL\n");
        return;
    }
    for(int i=leftOver;i<sizeOfArray;i++){
        //fprintf(stderr,"Checking if array has elements\n");
        if(arrayOfSmallerResults[i]->totalElements!=0){
            //fprintf(stderr,"list number #%d connected\n",i);
            //arrayOfSmallerResults[0]->Tail->next =
            (*resultsR)->Tail->next = arrayOfSmallerResults[i]->Head;
            (*resultsR)->Tail = arrayOfSmallerResults[i]->Tail;
            (*resultsR)->size += arrayOfSmallerResults[i]->size;
            (*resultsR)->totalElements += arrayOfSmallerResults[i]->totalElements;
            //fprintf(stderr,"Added non empty result from job #%d \n",i);

        }
        free(arrayOfSmallerResults[i]);
    }
}

void printList(list* listToPrint){
    int size=listToPrint->size;
    //fprintf(debugFile,"Hello i am printingList %d\n",listToPrint->relationId);
    //fprintf(debugFile,"It has %d nodes and a total of %d elements\n\n",listToPrint->size,listToPrint->totalElements);
    for(int i=0;i<size;i++){
        simpleNode* temp = listToPrint->Head;
        int position=0;
        while(position!=temp->index){
            //fprintf(debugFile,"Element = %ld \n",temp->result[position]);
            position++;
        }
        temp=temp->next;
    }
    //fprintf(debugFile,"Print Finished\n\n");
}


void DeleteList(list* myList){
   
    while(myList->Head != NULL){
        simpleNode* tempNode = myList->Head->next;
        free(myList->Head->result);
        free(myList->Head);
        myList->Head = tempNode;
    }
    free(myList);
}


void DeleteHeadList(headList* myHeadList){
    while(myHeadList->pointertoFirstList !=NULL){
        node* tempNode = myHeadList->pointertoFirstList->nextList;
        DeleteList(myHeadList->pointertoFirstList->List);
        free(myHeadList->pointertoFirstList);
        myHeadList->pointertoFirstList = tempNode;
    }
    free(myHeadList);
}
