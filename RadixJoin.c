#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "StoreRelation.h"
#include "List.h"
#include "RadixJoin.h"
#include "HashTable.h"
#include "Index.h"
#include "Scheduler.h"


//final


int totalJobs;


void createTuple(tuple* myTuple,uint64_t key, uint64_t payload){

    myTuple -> key = key;
    myTuple -> payload = payload;

}


void createRelation(relation* myColumn,uint64_t* columnRequested,int numRows){

    myColumn -> tuples = malloc(numRows * sizeof(tuple));
	
    for(int i = 0; i < numRows; i++){

        myColumn -> tuples[i].key = columnRequested[i];
        myColumn -> tuples[i].payload = i; 
    }
    myColumn -> num_tuples = numRows;
}




void findingAllTuples(list* head1,list* head2,int** bucket,int** myChain,relation* S,relation* Rtonos,int* p,int startPointS,int endPointS){
    for(int i=startPointS; i< endPointS; i++){
        int hash1Value = hashFunction1(S->tuples[i].key);
        
        int hash2Value = hashFunction2(S->tuples[i].key);
        //printf("hashValue1 is %d  hashValue2 is %d of Value %d\n",hash1Value,hash2Value,S->tuples[i].key);
        int index = bucket[hash1Value][hash2Value]-1;          // position where bucket shows
        if(index<0) {
            continue;
        }
       // printf("index is %d\n",index);
        if(Rtonos->tuples[index].key == S->tuples[i].key){
            //printf("FOUND MATCH\n");
           // printf("%d and %d  == %d and %d\n",Rtonos->tuples[index].key,Rtonos->tuples[index].payload,S->tuples[i].key,S->tuples[i].payload);
            //fprintf(stderr,"FOUND RESULT NOOB!\n");
            pushResult(head1,S->tuples[i].payload);
            pushResult(head2,Rtonos->tuples[index].payload);
        }
        
        while(myChain[hash1Value][index-p[hash1Value]] >0){
            index = myChain[hash1Value][index-p[hash1Value]]-1;
            //printf("index in while is %d and Rtonos->tuples[index].payload %d\n",index,Rtonos->tuples[index].payload);
            if(Rtonos->tuples[index].key == S->tuples[i].key){
                //printf("%d and %d  == %d and %d\n",Rtonos->tuples[index].key,Rtonos->tuples[index].payload,S->tuples[i].key,S->tuples[i].payload);
                //printf("FOUND MATCH\n");
                //fprintf(stderr,"FOUND RESULT NOOB!\n");
                pushResult(head1,S->tuples[i].payload);
                pushResult(head2,Rtonos->tuples[index].payload);
            }
            
        }

    }
}

void RadixHashJoin(list* resultsR,list* resultsS,relation* R,relation* S,StoreTable TableR,StoreTable TableS,int columnIdR,int columnIdS){
   // fprintf(stderr,"WELCOME TO RADIX_HASH_JOIN\n");
    int total_buckets =(int) pow(2,N);
    int whoHasIndex=0;
    int sizeR=R->num_tuples;
    int sizeS=S->num_tuples;

    int* hR = (int*) malloc(total_buckets*sizeof(int));
    int* hS = (int*) malloc(total_buckets*sizeof(int));
    for(int i=0;i<total_buckets;i++){
        hR[i]=0;
        hS[i]=0;
    }
    //initializing jobs for histogram
    totalJobs=NUM_OF_THREADS;
    job* jobArray = (job*) malloc(NUM_OF_THREADS*sizeof(job));
    argumentForHist** arg = (argumentForHist**) malloc(NUM_OF_THREADS*sizeof(argumentForHist*));;
    int** histogramsOfR = (int**) malloc(NUM_OF_THREADS*sizeof(int*));
    int** histogramsOfS = (int**) malloc(NUM_OF_THREADS*sizeof(int*));
    for(int i=0;i<NUM_OF_THREADS;i++){
        arg[i] = (argumentForHist*) malloc(sizeof(argumentForHist));
        histogramsOfR[i] = (int*) malloc(total_buckets*sizeof(int));
        histogramsOfS[i] = (int*) malloc(total_buckets*sizeof(int));
        for(int j=0;j<total_buckets;j++){
            histogramsOfR[i][j]=0;
            histogramsOfS[i][j]=0;
        }
        arg[i]->R=R;
        arg[i]->S=S;
        arg[i]->HistR = histogramsOfR[i];
        arg[i]->HistS = histogramsOfS[i];
        arg[i]->id=i;
        arg[i]->startPointR = i*(sizeR/NUM_OF_THREADS);
        arg[i]->endPointR = i*(sizeR/NUM_OF_THREADS) + (sizeR/NUM_OF_THREADS);
        arg[i]->startPointS = i*(sizeS/NUM_OF_THREADS);
        arg[i]->endPointS = i*(sizeS/NUM_OF_THREADS) + (sizeS/NUM_OF_THREADS);
        if(i==(NUM_OF_THREADS-1)){
            arg[i]->endPointS = arg[i]->endPointS + sizeS % NUM_OF_THREADS;
            arg[i]->endPointR = arg[i]->endPointR + sizeR % NUM_OF_THREADS;
        }
        void* argument = (void*) arg[i];
        jobArray[i].function = &HistogramJob;
        jobArray[i].argument = argument;
        place(&queue,jobArray[i]);
    }

    //waiting for all to complete their histograms
    Barrier(totalJobs);
    combineHistograms(hR,histogramsOfR,NUM_OF_THREADS,total_buckets);
    combineHistograms(hS,histogramsOfS,NUM_OF_THREADS,total_buckets);
    for(int i=0; i<NUM_OF_THREADS;i++){
        free(arg[i]);
        free(histogramsOfR[i]);
        free(histogramsOfS[i]);

    }
    free(histogramsOfR);
    free(histogramsOfS);
    free(arg);

    //fprintf(stderr,"\n\n\n #1 BARRIER PASSED \n\n\n");
    int* p = createPsum(hR);
    int* ps = createPsum(hS);
    //fprintf(stderr,"Left psum\n");
    
    //making copies of pSum to create the Reordered
    int* pCopy = (int*) malloc(total_buckets*sizeof(int));
    int* psCopy = (int*) malloc(total_buckets*sizeof(int));
    for(int i=0;i<total_buckets;i++){
        pCopy[i] = p[i];
        psCopy[i] = ps[i];
    } 
    
    
    
    //initialize jobs for reordering
    relation* Rtonos= (relation*) malloc(sizeof(relation));
    Rtonos->tuples = (tuple*) malloc(R->num_tuples * sizeof(tuple));
    Rtonos->num_tuples = R->num_tuples;
    for(int i=0;i<R->num_tuples;i++){
        Rtonos->tuples[i].key=0;
        Rtonos->tuples[i].payload=0;
    }
    relation* Stonos= (relation*) malloc(sizeof(relation));
    Stonos->tuples = (tuple*) malloc(S->num_tuples * sizeof(tuple));
    Stonos->num_tuples = S->num_tuples;
    for(int i=0;i<S->num_tuples;i++){
        Stonos->tuples[i].key=0;
        Stonos->tuples[i].payload=0;
    }
    argumentForReordering** argR = (argumentForReordering**) malloc(NUM_OF_THREADS*sizeof(argumentForReordering*));
    for(int i=0;i<NUM_OF_THREADS;i++){
        argR[i] = (argumentForReordering*) malloc(sizeof(argumentForReordering));
        argR[i]->R=R;
        argR[i]->S=S;
        argR[i]->psumR=pCopy;
        argR[i]->psumS=psCopy;
        argR[i]->Rtonos= Rtonos;
        argR[i]->Stonos= Stonos;
        argR[i]->id=i;
        argR[i]->startPointR = i*(sizeR/NUM_OF_THREADS);
        argR[i]->endPointR = i*(sizeR/NUM_OF_THREADS) + (sizeR/NUM_OF_THREADS);
        argR[i]->startPointS = i*(sizeS/NUM_OF_THREADS);
        argR[i]->endPointS = i*(sizeS/NUM_OF_THREADS) + (sizeS/NUM_OF_THREADS);
        if(i==(NUM_OF_THREADS-1)){
            argR[i]->endPointS = argR[i]->endPointS + sizeS % NUM_OF_THREADS;
            argR[i]->endPointR = argR[i]->endPointR + sizeR % NUM_OF_THREADS;
        }
        void* argument = (void*) argR[i];
        jobArray[i].function = &PartitionJob;
        jobArray[i].argument = argument;
        place(&queue,jobArray[i]);
    }
    //fprintf(stderr,"Left arg reord\n");
    //waiting for all to calculate reordering
    Barrier(totalJobs);
    for(int i=0;i<NUM_OF_THREADS;i++){
        free(argR[i]);
    }
    free(argR);
    //fprintf(stderr,"\n\n\n #2 BARRIER PASSED \n\n\n");
    free(jobArray);
    //createReorderedRelation(pCopy,R,0,sizeR,Rtonos);
    //createReorderedRelation(psCopy,S,0,sizeS,Stonos);
    free(pCopy);
    free(psCopy);
    int** bucket = (int**) malloc(total_buckets*sizeof(int*));
    int** myChain = (int**) malloc(total_buckets*sizeof(int*));

    if(R->num_tuples>=S->num_tuples){
        for(int bucketNum=0;bucketNum<total_buckets;bucketNum++){
            createIndex(Rtonos,&bucket[bucketNum],&myChain[bucketNum],p,bucketNum);
        }
    }
    else{
        for(int bucketNum=0;bucketNum<total_buckets;bucketNum++){
            createIndex(Stonos,&bucket[bucketNum],&myChain[bucketNum],ps,bucketNum);
        }
        whoHasIndex = 1;
    }
   // fprintf(stderr,"Who has index = %d\n",whoHasIndex);
    job* joinJobArray = (job*) malloc(total_buckets*sizeof(job));
    argumentForJoin** argJ = (argumentForJoin**) malloc(total_buckets*sizeof(argumentForJoin*));
    list** arrayOfThreadResultsForR = (list**) malloc(total_buckets*sizeof(list*));
    list** arrayOfThreadResultsForS = (list**) malloc(total_buckets*sizeof(list*));
    totalJobs=total_buckets;
    if(whoHasIndex==0){
        for(int i=0;i<total_buckets;i++){
            argJ[i] = (argumentForJoin*) malloc(sizeof(argumentForJoin));
            argJ[i]->id = i;
            createList(&arrayOfThreadResultsForR[i],resultsR->relationId);
            createList(&arrayOfThreadResultsForS[i],resultsR->relationId);
            argJ[i]->resultsOfIndexed = arrayOfThreadResultsForR[i];
            argJ[i]->resultsOfS = arrayOfThreadResultsForS[i] ;
            argJ[i]->bucket=bucket;
            argJ[i]->myChain=myChain;
            argJ[i]->Relation=Stonos;
            argJ[i]->RelationWithIndex=Rtonos;
            if(i==(total_buckets-1)){
                argJ[i]->startPointS=ps[i];
                argJ[i]->endPointS=Stonos->num_tuples;
            }
            else{
                argJ[i]->startPointS=ps[i];
                argJ[i]->endPointS=ps[i+1];
            }
            argJ[i]->psumWithIndex=p;
            void* argument = (void*) argJ[i];
            joinJobArray[i].function = &JoinJob;
            joinJobArray[i].argument = argument;
            //fprintf(stderr,"Job #%d placed\n",i);
            place(&queue,joinJobArray[i]);
        }
    }
    else{
        for(int i=0;i<total_buckets;i++){
            argJ[i] = (argumentForJoin*) malloc(sizeof(argumentForJoin));
            argJ[i]->id = i;
            createList(&(arrayOfThreadResultsForR[i]),0);
            createList(&(arrayOfThreadResultsForS[i]),0);
            argJ[i]->resultsOfIndexed = arrayOfThreadResultsForS[i];
            argJ[i]->resultsOfS = arrayOfThreadResultsForR[i] ;
            argJ[i]->bucket=bucket;
            argJ[i]->myChain=myChain;
            argJ[i]->Relation=Rtonos;
            argJ[i]->RelationWithIndex=Stonos;
            if(i==(total_buckets-1)){
                argJ[i]->startPointS=p[i];
                argJ[i]->endPointS=Rtonos->num_tuples;
            }
            else{
                argJ[i]->startPointS=p[i];
                argJ[i]->endPointS=p[i+1];
            }
            argJ[i]->psumWithIndex=ps;
            void* argument = (void*) argJ[i];
            joinJobArray[i].function = &JoinJob;
            joinJobArray[i].argument = argument;
            //fprintf(stderr,"Placing Job #%d\n",i);
            place(&queue,joinJobArray[i]);
        }
    }
   // fprintf(stderr,"Hitting Barrier\n");
    Barrier(totalJobs);
    free(joinJobArray);
   // fprintf(stderr,"\n\n\n #3 BARRIER PASSED \n\n\n");
    combineResults(&resultsR,arrayOfThreadResultsForR,total_buckets);
    //printList(resultsR);
    combineResults(&resultsS,arrayOfThreadResultsForS,total_buckets);
    //printList(resultsS);
    free(arrayOfThreadResultsForR);
    free(arrayOfThreadResultsForS);
    for(int i=0;i<total_buckets;i++){
        free(argJ[i]);
    }
    free(argJ);
    free(Rtonos->tuples);
    free(Stonos->tuples);
    free(Rtonos);
    free(Stonos);
    free(hR);
    free(hS);
    free(p);
    free(ps);
    for(int i=0;i<total_buckets;i++){
        free(myChain[i]);
        free(bucket[i]);
    }
    free(bucket);
    free(myChain); 
    return;

}

void iterateJoin(list* resultsOfIntermediate,relation R,relation S){
    //fprintf(stderr,"start iteratejoin\n");
    int size=R.num_tuples;

    for(int i=0;i<size;i++){
        if(R.tuples[i].key==S.tuples[i].key){
            pushResult(resultsOfIntermediate,R.tuples[i].payload);
        }
    }
    
    return;
}