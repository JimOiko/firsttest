#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "StoreRelation.h"
#include "JoinEnumeration.h"
#include "Statistics.h"
#include "Input.h"

void StatsOfFilters(uint64_t num,int filterType,ChangingStats* TableTofilter,int columnId,int storeTableNumOfColumns){
    uint64_t prevf = TableTofilter[columnId].f;
    //fprintf(stderr,"i,u,d,f beggining with columnid %d %d %d %d %d\n",columnId,(int)TableTofilter[columnId].i,(int)TableTofilter[columnId].u,(int)TableTofilter[columnId].d,(int)TableTofilter[columnId].f);

    //FILTER R.a = NUM
    if(filterType == 2){
        //fprintf(stderr,"lowest vlaue is %ld and distinct values %d\n",TableTofilter[columnId].i,TableTofilter[columnId].d);
        int posinArray = (num-TableTofilter[columnId].i) / 8;
        
        TableTofilter[columnId].i = num;
        TableTofilter[columnId].u = num;
        
        unsigned char mask = 0x80; //10000000
        mask = mask >> (num-TableTofilter[columnId].i) % 8; 
        unsigned char isTrue = mask & TableTofilter[columnId].distinct[posinArray];
        isTrue = isTrue << (num-TableTofilter[columnId].i) % 8;
        //fprintf(stderr,"ISTRUE %d\n",isTrue);
        if(isTrue == 128){

            TableTofilter[columnId].f = calculateFilterf2(TableTofilter[columnId].f,TableTofilter[columnId].d);
            TableTofilter[columnId].d = 1;
        }
        else{
            TableTofilter[columnId].f = 0;
            TableTofilter[columnId].d = 0;
        }
        for(int i=0;i<storeTableNumOfColumns;i++){
            if(i != columnId){
                if(TableTofilter[columnId].f==0){
                    TableTofilter[i].d=0;
                    TableTofilter[i].f=0;
                }
                else{

                    TableTofilter[i].d = calculateFilterColumnd2(TableTofilter[columnId].f,prevf,TableTofilter[i].f,TableTofilter[i].d);
                    TableTofilter[i].f = TableTofilter[columnId].f;
                }
            }
        }
    }
    //FILTER R.a > NUM
    else if(filterType == 0){
        uint64_t previ = TableTofilter[columnId].i;
        if(num < TableTofilter[columnId].i){
            num = TableTofilter[columnId].i;
        }
        TableTofilter[columnId].i = num;
        TableTofilter[columnId].f = calculateFilterf0(TableTofilter[columnId].u,num,previ,TableTofilter[columnId].f);

        TableTofilter[columnId].d = calculateFilterd0(TableTofilter[columnId].u,num,previ,TableTofilter[columnId].d);
        for(int i=0;i<storeTableNumOfColumns;i++){
            if(i != columnId){

                TableTofilter[i].d=calculateFilterColumnd0(TableTofilter[columnId].f,prevf,TableTofilter[i].f,TableTofilter[i].d);
                TableTofilter[i].f = TableTofilter[columnId].f;
            }
        }
    }
    //FILTER R.a < NUM
    else if(filterType == 1){

        uint64_t prevu = TableTofilter[columnId].u;
        
        if(num > TableTofilter[columnId].u){
            num = TableTofilter[columnId].u;
        }
        TableTofilter[columnId].u = num;

        TableTofilter[columnId].f = calculateFilterf1(num,TableTofilter[columnId].i,prevu,TableTofilter[columnId].f);
        TableTofilter[columnId].d = calculateFilterd1(num,TableTofilter[columnId].i,prevu,TableTofilter[columnId].d);

        for(int i=0;i<storeTableNumOfColumns;i++){
            if(i != columnId){

                TableTofilter[i].d = calculateFilterColumnd0(TableTofilter[columnId].f,prevf,TableTofilter[i].f,TableTofilter[i].d);
                TableTofilter[i].f = TableTofilter[columnId].f;
            }
            //fprintf(stderr,"AFTER FILTER THINGS ARE LIKE THIS in column %d:\ni,u,d,f after %d %d %d %d\n",i,(int)TableTofilter[i].i,(int)TableTofilter[i].u,(int)TableTofilter[i].d,(int)TableTofilter[i].f);
        }
    }
}


void StatisticsInRadix(int numOfColumnsR,int numOfColumnsS,ChangingStats* TableforStatsR,ChangingStats* TableforStatsS,int columnIdR,int columnIdS){
    uint64_t min,max;
    if(TableforStatsR[columnIdR].i > TableforStatsS[columnIdS].i){
        max = TableforStatsR[columnIdR].i; 
    }
    else{
        max = TableforStatsS[columnIdS].i; 
    }
    if(TableforStatsR[columnIdR].u < TableforStatsS[columnIdS].u){
        min = TableforStatsR[columnIdR].u; 
    }
    else{
        min = TableforStatsS[columnIdS].u; 
    }
    TableforStatsR[columnIdR].i = max;
    TableforStatsS[columnIdS].i = max;
    TableforStatsR[columnIdR].u = min;
    TableforStatsS[columnIdS].u = min;
    unsigned int prevdR = TableforStatsR[columnIdR].d; 
    unsigned int prevdS =TableforStatsS[columnIdS].d;
    if(TableforStatsR[columnIdR].f==0){
        TableforStatsR[columnIdR].f=0;
        TableforStatsS[columnIdS].f=0;
        TableforStatsR[columnIdR].d=0;
        TableforStatsS[columnIdS].d=0;
    }
    else{

        TableforStatsR[columnIdR].f = calculateRadixf(TableforStatsR[columnIdR].u,TableforStatsR[columnIdR].i,TableforStatsR[columnIdR].f,TableforStatsR[columnIdS].f);
        TableforStatsS[columnIdS].f =  TableforStatsR[columnIdR].f;

        TableforStatsR[columnIdR].d = calculateRadixd(TableforStatsR[columnIdR].u,TableforStatsR[columnIdR].i,TableforStatsR[columnIdR].d,TableforStatsR[columnIdS].d);
        TableforStatsS[columnIdS].d = TableforStatsR[columnIdR].d;
    }
    for(int i=0;i<numOfColumnsR;i++){
        if(i != columnIdR){
            if(TableforStatsR[columnIdR].f==0){
                TableforStatsR[i].d=0;
                TableforStatsR[i].f=0;
            }
            else{
                TableforStatsR[i].d = calculateColumnsRadixd(TableforStatsR[columnIdR].d,prevdR,TableforStatsR[i].f,TableforStatsR[i].d);
                TableforStatsR[i].f = TableforStatsR[columnIdR].f;
            }
        }
    }
    for(int i=0;i<numOfColumnsS;i++){
        if(i != columnIdS){
            if(TableforStatsS[columnIdS].f==0){
                TableforStatsS[i].d=0;
                TableforStatsS[i].f=0;
            }
            else{
                TableforStatsS[i].d = calculateColumnsRadixd(TableforStatsS[columnIdS].d,prevdS,TableforStatsS[i].f,TableforStatsS[i].d);
                TableforStatsS[i].f = TableforStatsS[columnIdS].f;
            }
        }
    }
   // fprintf(stderr,"\nBGHKA Statistics in radix\n");
}

void StatsforSameTableCase(StoreTable TableToJoin,ChangingStats* TableforStatsR,int columnIdR,int columnIdS){
    //fprintf(stderr,"\nmphka same statsforsametable\n");
    int prevf = TableforStatsR[columnIdR].f;
    uint64_t min,max;
    if(columnIdR != columnIdS){
        if(TableforStatsR[columnIdR].i > TableforStatsR[columnIdS].i){
            max = TableforStatsR[columnIdR].i; 
        }
        else{
            max = TableforStatsR[columnIdS].i; 
        }
        if(TableforStatsR[columnIdR].u < TableforStatsR[columnIdS].u){
            min = TableforStatsR[columnIdR].u; 
        }
        else{
            min = TableforStatsR[columnIdS].u; 
        }
        TableforStatsR[columnIdR].i = max;
        TableforStatsR[columnIdS].i = TableforStatsR[columnIdR].i;
        TableforStatsR[columnIdR].u = min;
        TableforStatsR[columnIdS].u = TableforStatsR[columnIdR].u;
        float n =TableforStatsR[columnIdR].u-TableforStatsR[columnIdR].i+1;
        TableforStatsR[columnIdR].f = (float) TableforStatsR[columnIdR].f/n;
        if(TableforStatsR[columnIdR].f==0 && TableforStatsR[columnIdR].d!=0){
            TableforStatsR[columnIdR].f=1;
        }
        TableforStatsR[columnIdS].f = TableforStatsR[columnIdR].f; 
        if(TableforStatsR[columnIdR].f==0){
            TableforStatsR[columnIdR].d=0;
        }
        else{
            float base = (float) TableforStatsR[columnIdS].f/ (float) prevf;
            float exponent = (float) prevf/ (float) TableforStatsR[columnIdR].d;
            TableforStatsR[columnIdR].d = (unsigned int) ((float) TableforStatsR[columnIdR].d * (1-pow(1.0-base,exponent)));
            if(TableforStatsR[columnIdR].d==0){
                TableforStatsR[columnIdR].d=1;
            }
        }
        TableforStatsR[columnIdS].d =TableforStatsR[columnIdR].d;
        for(int i=0;i<TableToJoin.numOfColumns;i++){
            if(i != columnIdR && i != columnIdS ){
                if(TableforStatsR[columnIdR].f==0){
                    TableforStatsR[i].d=0;
                }
                else{
                    float base = (float) TableforStatsR[columnIdS].f/ (float) prevf;
                    float exponent= (float) TableforStatsR[i].f/ (float) TableforStatsR[i].d;
                    TableforStatsR[i].d = (unsigned int) ((float) TableforStatsR[i].d *(1-pow(1.0-base,exponent))); 
                    if(TableforStatsR[columnIdR].d==0){
                        TableforStatsR[columnIdR].d=1;
                    }
                }
                TableforStatsR[i].f = TableforStatsR[columnIdR].f;
            }
        }
       // fprintf(stderr,"finish iteratejoin\n");
    }
    else{   //same table same Column
        float n = TableforStatsR[columnIdR].u - TableforStatsR[columnIdR].i + 1;
        float numenator = TableforStatsR[columnIdR].f* TableforStatsR[columnIdR].f;
        TableforStatsR[columnIdR].f = (unsigned int) numenator / n ;
        if(TableforStatsR[columnIdR].f==0 && TableforStatsR[columnIdR].d!=0){
            TableforStatsR[columnIdR].f=1;
        }
        for(int i=0;i<TableToJoin.numOfColumns;i++){
            if(i != columnIdR ){
                TableforStatsR[i].f = TableforStatsR[columnIdR].f;
            }
        }
    }
    //fprintf(stderr,"\nBghka same statsforsametable\n");

}

unsigned int hashFunctionForBestTree(unsigned int* relationIdArray,int numberOfRelations){
    unsigned int hash=0;
    for(int i=0;i<numberOfRelations;i++){
        unsigned int base=1;
        base = base << relationIdArray[i];
        hash = hash | base;        
    }
    return hash;
}


bestTree createBestTree(int** relationIdArray,unsigned int numberOfRelations,unsigned int* cost){
    bestTree newBestTree;
    unsigned int size=1;
    size = size << numberOfRelations;
    newBestTree.path = (int**) malloc(size*sizeof(int*));
    newBestTree.cost = (unsigned int*) malloc(size*sizeof(unsigned int));
    for(int i=0;i<size;i++){
        newBestTree.path[i]=NULL;
        newBestTree.cost[i] = 0;
    }
    for(int i=0;i<numberOfRelations;i++){
        unsigned int pos = hashFunctionForBestTree((unsigned int*) relationIdArray[i],1);
        newBestTree.path[pos] = (int*) malloc(sizeof(int));
        newBestTree.path[pos][0] = i;
        newBestTree.cost[pos] = cost[i];
    }
    return newBestTree;
}

void deleteBestTree(bestTree myBestTree,int numberOfRelations){
    for(int i=0;i<numberOfRelations;i++){
        free(myBestTree.path[i]);
    }
    free(myBestTree.path);
    free(myBestTree.cost);
}

unsigned int** GetAllSubsetOfSizeR(int* set,int r,int setSize,int* totalSubsets){
    unsigned int** subsetArray;
    int numenator = factorialUtil(setSize);
    int denumenator1 = factorialUtil(r);
    int denumenator2 = factorialUtil(setSize-r);
    int denumenator = denumenator1*denumenator2;
    *totalSubsets = numenator/denumenator;
    subsetArray= (unsigned int**) malloc((*totalSubsets)*sizeof(unsigned int*));
    unsigned int* buffer;
    for(int i=0;i<(*totalSubsets);i++){
        subsetArray[i]= (unsigned int*) malloc(r*sizeof(unsigned int));
    }
    buffer = (unsigned int*) malloc(r*sizeof(unsigned int));
    int pos = 0;
    findCombinations(set,setSize,r,0,subsetArray,0,&pos,buffer);
    free(buffer);
    return subsetArray;
}


void findCombinations(int* set,int setSize,int r,int index, unsigned int ** subsetArray,int i,int* pos, unsigned int* buffer){
    if (index == r) { 
        memcpy(subsetArray[*pos],buffer,r*sizeof(unsigned int));

        (*pos)++;
        return; 
    } 
  
    if (i >= setSize){
        return; 
    }
    buffer[index] = set[i]; 
    findCombinations(set, setSize , r, index + 1, subsetArray, i + 1,pos,buffer); 
  
    findCombinations(set, setSize , r, index, subsetArray, i + 1,pos,buffer); 
}


int factorialUtil(int number){
    int factorial=1;
    for(int i=1;i<=number;i++){
        factorial *= i;
    }
    return factorial;
}

int checkIfIdisInSet(int idToCheck,unsigned int* set,int setSize){
    for(int i=0; i<setSize;i++){
        if(idToCheck==set[i]){
            return 1;
        }
    }
    return 0;
}

int checkIfConnected(int idToCheck,unsigned int* set,int setSize,singlePredicate* predicateArray,int predicateArraySize){
    for(int i=0;i<predicateArraySize;i++){
        if(predicateArray[i].typeOfPredicate==0){
            char* id1;
            char* id2;
            char* temp2= (char*) malloc((strlen(predicateArray[i].predicate)+1)*sizeof(char));
            strcpy(temp2,predicateArray[i].predicate);
            getRelationIdOfPredicate(&id1,&id2,temp2);
            if(idToCheck==atoi(id1)){
                if(checkIfIdisInSet(atoi(id2),set,setSize)){
                    free(id1);
                    free(id2);
                    free(temp2);
                    return i;
                }
                free(id1);
                free(id2);
                free(temp2);
            }
            else if(idToCheck==atoi(id2)){
                if(checkIfIdisInSet(atoi(id1),set,setSize)){
                    free(id1);
                    free(id2);
                    free(temp2);
                    return i;
                }
            }
            else{
                free(id1);
                free(id2);
                free(temp2);
                continue;
            } 
        }
    }
    return -1;
}

int checkForCrossProducts(unsigned int* set,int setSize,singlePredicate* predicateArray,int predicateArraySize){
    unsigned int* subset = (unsigned int*) malloc((setSize-1)*sizeof(unsigned int));
    if(setSize==1){
        free(subset);
        return 0;
    }
    for(int i=0; i<setSize;i++){
        unsigned int idToCheck = set[i];
        for(int j=0;j<i;j++){
            subset[j] = set[j];
        }
        for(int j=i+1;j<setSize;j++){
            subset[j-1] = set[j];
        }
        int pos = checkIfConnected(idToCheck,subset,setSize-1,predicateArray,predicateArraySize);
        if(pos==-1){
            free(subset);
            return 1;
        }
       // fprintf(stderr,"They are connected in %s\n",predicateArray[pos].predicate);
    }
    free(subset);
    return 0;
}


int* joinEnumerationAlgorithm(int* relationIdArray,int arraySize,singlePredicate* predicateArray,int totalPredicates,ChangingStats** statsToChange,StoreTable* TableOfArrays,int* tablesInQuestion){
    int** tempArray = (int**) malloc(arraySize*sizeof(int*));
    for(int i=0;i<arraySize;i++){
        tempArray[i] = (int*) malloc(sizeof(int));
        tempArray[i][0]=relationIdArray[i];
    }
    unsigned int* cost = (unsigned int*) malloc(arraySize*sizeof(unsigned int));
    for(int i=0;i<arraySize;i++){
        cost[i] = statsToChange[i][0].f;
    }
    //fprintf(stderr,"Creating bestTree\n");
    bestTree newBestTree = createBestTree(tempArray,arraySize,cost);
    free(cost);
    for(int i=0;i<arraySize;i++){
        free(tempArray[i]);
    }
    free(tempArray);
    //fprintf(stderr,"BestTree Created\n");
    for(int i=1;i<=arraySize;i++){
        int totalSubsets;
        unsigned int** subsets;
        subsets=GetAllSubsetOfSizeR(relationIdArray,i,arraySize,&totalSubsets);
        for(int j=0;j<totalSubsets;j++){
            for(int k=0;k<arraySize;k++){
                for(int l=0;l<i;l++){
                }
               if (checkIfIdisInSet(relationIdArray[k],subsets[j],i)){
                   continue;
               }
               int noCrossProducts = checkForCrossProducts(subsets[j],i,predicateArray,totalPredicates);
               int pos=checkIfConnected(relationIdArray[k],subsets[j],i,predicateArray,totalPredicates);
               if(pos==-1 || noCrossProducts){
                   continue;
               }
               unsigned int positionInBestTree = hashFunctionForBestTree(subsets[j],i);
               int relationIdR,relationIdS,columnIdR,columnIdS;
               getJoinInfo(predicateArray[pos].predicate,&relationIdR,&relationIdS,&columnIdR,&columnIdS);
               currTree currentTree;
     
               if(relationIdArray[k]==relationIdR){
                    currentTree = createJoinTree(newBestTree.cost[positionInBestTree],newBestTree.path[positionInBestTree],i,relationIdArray[k],
                                                        TableOfArrays[tablesInQuestion[relationIdArray[k]]].numOfColumns,
                                                        statsToChange[relationIdArray[k]],columnIdR,
                                                        TableOfArrays[tablesInQuestion[relationIdS]].numOfColumns,
                                                        statsToChange[relationIdS],columnIdS);
               }
               else{

                    currentTree = createJoinTree(newBestTree.cost[positionInBestTree],newBestTree.path[positionInBestTree],i,relationIdArray[k],
                                                        TableOfArrays[tablesInQuestion[relationIdArray[k]]].numOfColumns,
                                                        statsToChange[relationIdArray[k]],columnIdS,
                                                        TableOfArrays[tablesInQuestion[relationIdR]].numOfColumns,
                                                        statsToChange[relationIdR],columnIdR);
               }
               unsigned int* newSubset = (unsigned int*) malloc((i+1)*sizeof(unsigned int));
               for(int l=0;l<i;l++){
                    newSubset[l]=subsets[j][l];    
               }
               newSubset[i]=relationIdArray[k];
               int newPositionInBestTree = hashFunctionForBestTree(newSubset,i+1);
               free(newSubset);
               if(newBestTree.path[newPositionInBestTree]==NULL || newBestTree.cost[newPositionInBestTree] > currentTree.cost){
          
                    newBestTree.path[newPositionInBestTree] = currentTree.path;
                    newBestTree.cost[newPositionInBestTree] = currentTree.cost;
               }
            }
        }
        for(int j=0;j<totalSubsets;j++){
            free(subsets[j]);
        }
        free(subsets);
    }
    unsigned int size=1;
    size = size << (unsigned int) arraySize;

    int* arrayToReturn = (int*) malloc(arraySize*sizeof(int));

    memcpy(arrayToReturn,newBestTree.path[size-1],arraySize*sizeof(int));
    
    deleteBestTree(newBestTree,arraySize);
    return arrayToReturn;
}

currTree createJoinTree(unsigned int treeCost, int* CurrentPath,int pathSize, int newId,int numOfColumnsOfNew,ChangingStats* statsOfNew,int columnOfNew,int numOfColumnsOfOld,ChangingStats* statsOfOld,int columnOfOld){
    currTree newTree;
    newTree.path= (int*) malloc((pathSize+1)*sizeof(int));
   
    for(int i=0;i<pathSize;i++){

        newTree.path[i] = CurrentPath[i];
    }
    newTree.path[pathSize] = newId;
    StatisticsInRadix(numOfColumnsOfNew,numOfColumnsOfOld,statsOfNew,statsOfOld,columnOfNew,columnOfOld);
    newTree.cost = statsOfNew[0].f;
    return newTree;
}


void getNewPredicateArray(singlePredicate*newpredicateArray,int* size,singlePredicate* predicateArray, int*rightOrderArray,int sizeofOrder, int numberOfPredicates){
    int newcounter = 0;
    int indexinSet = 0;
    int idtoCheck = 0;
    unsigned int* set = (unsigned int*) malloc(sizeofOrder*sizeof(unsigned int));
    for(int i=0; i<sizeofOrder;i++){
        set[i]=0;
    }
    int* arrayReturned;
    int sizereturned;
    while(indexinSet+1 < sizeofOrder){

        set[indexinSet] = rightOrderArray[indexinSet];
        idtoCheck = rightOrderArray[indexinSet+1];
        indexinSet++;
        arrayReturned = arraycheckIfConnected(idtoCheck,set,indexinSet+1,predicateArray, numberOfPredicates, &sizereturned);
        for(int i =0; i<sizereturned ; i++){
            newpredicateArray[newcounter].typeOfPredicate=0;
            newpredicateArray[newcounter].predicate = (char*)malloc((strlen(predicateArray[arrayReturned[i]].predicate)+1)*sizeof(char));
            memcpy(newpredicateArray[newcounter].predicate,predicateArray[arrayReturned[i]].predicate,(strlen(predicateArray[arrayReturned[i]].predicate)+1)*sizeof(char));
            newcounter++;
        }
 
        free(arrayReturned);
    }
    free(set);
    *size=newcounter;
}

int* arraycheckIfConnected(int idToCheck,unsigned int* set,int setSize,singlePredicate* predicateArray,int predicateArraySize,int*sizetoreturn){
    int* arraytoreturn = malloc(predicateArraySize*sizeof(int));
    int counter = 0;

    for(int i=0;i<predicateArraySize;i++){
        char* id1;
        char* id2;
        if(predicateArray[i].typeOfPredicate==0){
            char* temp2= (char*) malloc((strlen(predicateArray[i].predicate)+1)*sizeof(char));
            strcpy(temp2,predicateArray[i].predicate);
            getRelationIdOfPredicate(&id1,&id2,temp2);
            if(idToCheck==atoi(id1)){
                if(checkIfIdisInSet(atoi(id2),set,setSize)){
                    arraytoreturn[counter] = i;
                    counter++;
                }
            }
            else if(idToCheck==atoi(id2)){
                if(checkIfIdisInSet(atoi(id1),set,setSize)){
                    arraytoreturn[counter] = i;
                    counter++;
                }
            }
            else{
                free(id1);
                free(id2);
                 free(temp2);
                continue;
            }
            
        }
    }
    (*sizetoreturn) = counter;
    return arraytoreturn;
}