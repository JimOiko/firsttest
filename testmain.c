#include <stdio.h>
#include <stdlib.h>
#include "JoinEnumeration.h"


int main(){
    int set[] = {0,1,2,3,4};
    int** subsetArray;
    for(int i=1;i<=5;i++){
        int totalSubsets;
        subsetArray=GetAllSubsetOfSizeR(set,i,5,&totalSubsets);
        for(int k=0;k<totalSubsets;k++){
            printf("Set #%d: ",k);
            fflush(stdout);
            for(int j=0;j<i;j++){
                printf("%d ",subsetArray[k][j]);
                fflush(stdout);
            }
            printf("\n");
        }
        for(int k=0;k<totalSubsets;k++){
            free(subsetArray[k]);
        }
        free(subsetArray);
    }
    return 0;
}