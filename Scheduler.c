#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <ctype.h> /* toupper */
#include <signal.h> /* signal */
#include <time.h>
#include <errno.h>
#include "StoreRelation.h"
#include "RadixJoin.h"
#include "List.h"
#include "Scheduler.h"
#include "HashTable.h"


int Endofrequests = 0;
int Runningthread = 0;
int numOfJobsExecuted;


void initialize ( schedulerQueue * queue ){
	queue -> data = (jobList*) malloc(sizeof(jobList));
	queue -> data -> start = NULL;
	queue -> data -> end = NULL;
	queue -> count = 0;	
}

void place ( schedulerQueue * queue , job task){
	pthread_mutex_lock (& mtx ) ;
	while(Runningthread == 1){
		//fprintf(stderr,"Stuck in wait for place\n");
		pthread_cond_wait(&cond_obtain,&mtx);
	}
	if (queue -> data -> end != NULL)
	{ 
		queue -> data -> end -> nextJob = (jobNode*) malloc(sizeof(jobNode));
		queue -> data -> end = queue -> data -> end ->nextJob;
	}
	else{
		queue -> data -> end = (jobNode*) malloc(sizeof(jobNode));
		fflush(stderr);

	}
	if ( queue -> count == 0)
	{
		queue -> data ->start = queue -> data -> end;
	}
	queue -> data -> end -> task = task;
	queue -> data -> end -> nextJob = NULL;
	queue -> count ++;
	pthread_mutex_unlock (& mtx );
	pthread_cond_signal(& cond_nonempty);
}

job obtain ( schedulerQueue * queue ){
	job task;

	pthread_mutex_lock (& mtx ) ;
	while ( queue -> count <= 0 || Runningthread == 1 ) {
		//fprintf (stderr," >> Found Buffer Empty with counter %d\n",queue->count);
		pthread_cond_wait (& cond_nonempty , & mtx );
		if (Endofrequests == 1)
		{
			pthread_mutex_unlock(&mtx);
			//fprintf(stderr,"Exiting thread % ld\n",pthread_self ());
			pthread_exit(0);
		}
	}
	Runningthread=1;

	task = queue -> data -> start -> task;
	if ( queue -> count == 1)
	{
		free(queue -> data -> start);
		queue -> data -> start =NULL;
		queue -> data -> end = NULL;
	}
	else{
		jobNode* temp = queue -> data -> start;
		queue -> data -> start = queue -> data -> start -> nextJob;
		free(temp);
	}
	queue -> count --;

	Runningthread = 0;
	pthread_mutex_unlock (& mtx );
	pthread_cond_signal(&cond_obtain);
	
	return task ;
}

void Barrier(int JobsaddedinQueue){
    pthread_mutex_lock (& barriermtx ) ;
    while ( numOfJobsExecuted < JobsaddedinQueue) {
        pthread_cond_wait (& cond_barrier , & barriermtx );
    }
    numOfJobsExecuted = 0;
    pthread_mutex_unlock (& barriermtx );
}


void DestroyList(schedulerQueue * queue){
	while (queue -> count > 0)
	{
		jobNode* temp = queue -> data -> start;
		queue -> data -> start = queue -> data -> start -> nextJob;
		free(temp);

		if (queue -> data -> start == NULL){
			queue -> data -> end = NULL;
		}
		queue -> count --;

	}
	free(queue -> data);
	//printf("List destroyed\n");
}

void* thread_routine(void* arg){
	while(1){
		job task = obtain(&queue);
		(*(task.function))(task.argument);

		pthread_mutex_lock(&anothermtx);
			numOfJobsExecuted++;
			if(numOfJobsExecuted == totalJobs){
				pthread_cond_signal(& cond_barrier);
			}
   		pthread_mutex_unlock (&anothermtx);
	}
}

void HistogramJob(void* argument){
	argumentForHist* arg = (argumentForHist*) argument;
	//fprintf(stderr,"thread is %ld and cut array from %d up to %d \n",pthread_self(),arg->startPointR,arg->endPointR);
	createHist(arg->R,arg->startPointR,arg->endPointR,arg->HistR);

	createHist(arg->S,arg->startPointS,arg->endPointS,arg->HistS);
	//fprintf(stderr,"finished hist\n");
}

void PartitionJob(void* argument){
	argumentForReordering* arg = (argumentForReordering*) argument;
	createReorderedRelation(arg->psumR,arg->R,arg->startPointR,arg->endPointR,arg->Rtonos);\

    createReorderedRelation(arg->psumS,arg->S,arg->startPointS,arg->endPointS,arg->Stonos);
	//fprintf(stderr,"finished reordered\n");
}


void JoinJob(void* argument){
	argumentForJoin* arg = (argumentForJoin*) argument;
	findingAllTuples(arg->resultsOfS,arg->resultsOfIndexed,arg->bucket,arg->myChain,arg->Relation,arg->RelationWithIndex,arg->psumWithIndex,arg->startPointS,arg->endPointS);

}