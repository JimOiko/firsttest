#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "StoreRelation.h"
#include "RadixJoin.h"
#include "HashTable.h"
#include "Index.h"


//final

int hashFunction2(int key){
    int hash=0;
    hash = key%101;
    if (hash<0){
        hash = hash*(-1);
    }
    return hash;
}


void createIndex(relation* R, int** bucket, int** myChain, int* p, int bucketNum){
    int sp,ep;
    int totalBucket = (int) pow(2,N);
    if(bucketNum==totalBucket-1){
        sp=p[bucketNum];
        ep=R->num_tuples;
    }
    else{
        sp=p[bucketNum];
        ep=p[bucketNum+1];
    }
    (*myChain) = (int*) malloc((ep-sp)*sizeof(int));
    for(int i=0;i<ep-sp;i++){
        (*myChain)[i]=0;
    }
    (*bucket) = (int*) malloc(101*sizeof(int));
    for(int i=0;i<101;i++){
        (*bucket)[i]=0;
    }
    for(int i=sp;i<ep;i++){
        int hashvalue = hashFunction2(R->tuples[i].key);
        if((*bucket)[hashvalue]==0){
            (*bucket)[hashvalue]=i+1;
        }
        else{
            (*myChain)[i-sp]=(*bucket)[hashvalue];
            (*bucket)[hashvalue]=i+1;
        }
    }
    return;
}

void printIndex(int num_tuples, int* bucket,int* myChain,int bucketNum,int* p){
    int sp,ep;
    int totalBuckets = (int) pow(2,N);
    if(bucketNum==totalBuckets-1){
        sp=p[bucketNum];
        ep=num_tuples;
    }
    else{
        sp=p[bucketNum];
        ep=p[bucketNum+1];
    }
    printf("%d %d sp ep \n",sp,ep);
    for(int i=0;i<101;i++){
        printf("bucket[%d] is %d\n",i,bucket[i]);
    }
    for(int i=0;i<ep-sp;i++){
        printf("chain[%d] has pointerToPosition %d\n",i,myChain[i]);
    }
    return;
}
